USE [TableBuilder]
GO
/****** Object:  StoredProcedure [dbo].[up_Get]    Script Date: 9/20/2018 6:31:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec up_Get @What='table-data',@ID='2',@Search='',@TopRecordCount=''
ALTER PROCEDURE [dbo].[up_Get](
	-- table-groups, tables-in-group, table-info
	@What VARCHAR(50),
	@ID VARCHAR(200) = '',
	@Search VARCHAR(200) = '',
	@TopRecordCount int='999999',
	@Username varchar(20) = ''
)
AS
BEGIN
	SET NOCOUNT ON;

	/*
---- Debugging:
DECLARE @What VARCHAR(50)='execute-data-view';
DECLARE @ID VARCHAR(200) = '641';
DECLARE @Search VARCHAR(200) = '';
DECLARE @TopRecordCount int='100'
-- */


	IF @What = 'execute-data-view' BEGIN

		DECLARE @SQLDataViewToExecute VARCHAR(MAX) =  (SELECT top 1
			ISNULL(big_data.Value, '')[value]
		FROM Config sql_data_view
			left outer join BigDataValues big_data on sql_data_view.DataLinkID = big_data.id
		where sql_data_view.ID = @ID and sql_data_view.Type = 'sql-data-view' )

		SET @SQLDataViewToExecute = REPLACE( @SQLDataViewToExecute, '{{TopRecordCount}}', @TopRecordCount);
		SET @SQLDataViewToExecute = REPLACE( @SQLDataViewToExecute, '{{SearchString}}', @Search);

		execute (@SQLDataViewToExecute)
	END


	--IF @What = 'execute-sql'
	--BEGIN

	--DECLARE @SQLToExecute VARCHAR(MAX) =  
	--(SELECT  top 1
	--ISNULL(row_field_value.Value, '') + ISNULL(big_data.Value, '') + ISNULL(very_big_data.Value, '') value
	--FROM     DataValues table_rows
	-- left outer join DataValues row_field_value on table_rows.id = row_field_value.RowID and row_field_value.Type = 'value'
	-- left outer join Config field on row_field_value.FieldID= field.id and field.Type = 'field' 
	-- left outer join Config field_type on field.id= field_type.parentid and field_type.Type = 'field-type' 
	-- left outer join BigDataValues big_data on row_field_value.bigdatavalueid = big_data.id
	-- left outer join VeryBigDataValues very_big_data on row_field_value.verybigdatavalueid = very_big_data.id
	--where table_rows.id = @ID and table_rows.Type = 'row' and field_type.DataLinkID=46)

	--SET @SQLToExecute = REPLACE( @SQLToExecute, '{{TopRecordCount}}', @TopRecordCount);
	--SET @SQLToExecute = REPLACE( @SQLToExecute, '{{SearchString}}', @Search);

	--execute (@SQLToExecute)
	--END

	IF @What = 'table-groups' BEGIN
		SELECT TOP(@TopRecordCount)
			CAST(tg.ID AS VARCHAR(50)) ID,
			tg.value Name,
			tgc.Value Comment,
			RTRIM(EmpGeneralCreated.EGenFirstName) + ISNULL(' ('+EmpGeneralCreated.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralCreated.EGenLastName) AS CreatedBy,
			tg.CreatedByID,
			FORMAT( tg.CreatedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS CreatedDate
		FROM Config tg
			join Config tg_rights on tg.id = tg_rights.ParentID
				and tg_rights.Type = 'table-group-right-viewer'
				and (tg_rights.Value = @Username or tg_rights.Value = 'Everyone')
			left outer join Config tgc on tg.id = tgc.ParentID and tgc.Type = 'table-group-comment'
			left outer join EIS.dbo.EmpGeneral EmpGeneralCreated ON tg.CreatedByID = EmpGeneralCreated.EGenEmpID
		WHERE tg.Type = 'table-group'
			AND (@Search = '' or tg.value like '%' + @Search + '%')
	END


	IF @What = 'table-group' BEGIN
		if (@ID IS NULL OR rtrim(ltrim(@ID)) = '') BEGIN
			raiserror('ID of table group is required.', 20, -1) with log
		END
		SELECT
			tg.ID ID,
			tg.value Name,
			tgc.Value Comment,
			RTRIM(EmpGeneralCreated.EGenFirstName) + ISNULL(' ('+EmpGeneralCreated.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralCreated.EGenLastName) AS CreatedBy,
			tg.CreatedByID,
			FORMAT( tg.CreatedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS CreatedDate
		FROM Config tg
			left outer join Config tgc on tg.id = tgc.ParentID and tgc.Type = 'table-group-comment'
			left outer join EIS.dbo.EmpGeneral EmpGeneralCreated ON tg.CreatedByID = EmpGeneralCreated.EGenEmpID
		WHERE tg.ID = @ID
			AND tg.Type = 'table-group'
	END

	IF @What = 'tables-in-group' BEGIN
		-- we need id
		IF (@ID IS NULL OR rtrim(ltrim(@ID)) = '') BEGIN
			RAISERROR('ID or table group is required.', 20, -1) WITH LOG
		END
		SELECT TOP(@TopRecordCount)
			CAST(tab.ID AS VARCHAR(50)) ID,
			tab.Value Name,
			comment.Value Comment,
			RTRIM(EmpGeneralCreated.EGenFirstName) + ISNULL(' ('+EmpGeneralCreated.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralCreated.EGenLastName) AS CreatedBy,
			tab.CreatedByID,
			FORMAT( tab.CreatedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS CreatedDate
		FROM Config[tgroup]
			JOIN Config[tab] ON tgroup.ID = tab.ParentID AND tab.Type = 'table'
			JOIN Config[tab_rights] ON tab.id = tab_rights.ParentID
				AND tab_rights.Type = 'table-right-viewer'
				AND (tab_rights.Value = @Username OR tab_rights.Value = 'Everyone')
			JOIN Config[comment] ON tab.id = comment.ParentID AND comment.Type='table-comment'
			LEFT OUTER JOIN EIS.dbo.EmpGeneral EmpGeneralCreated ON tab.CreatedByID = EmpGeneralCreated.EGenEmpID
		WHERE tgroup.id = @ID AND tgroup.Type = 'table-group'
	END


	IF @What = 'table-info' BEGIN
		if (@ID IS NULL OR rtrim(ltrim(@ID)) = '') BEGIN
			raiserror('ID of table is required.', 20, -1) with log
		END
		--declare @ID varchar(50)='608'
		SELECT
			CAST(tab.ID AS VARCHAR(50))[ID],
			tab.value[Name],
			comment.Value[Comment],
			big_sql_trigger_sql.value[Trigger],

			RTRIM(EmpGeneralCreated.EGenFirstName) + ISNULL(' ('+EmpGeneralCreated.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralCreated.EGenLastName) AS CreatedBy,
			tab.CreatedByID,
			FORMAT( tab.CreatedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS CreatedDate
		FROM Config[tab]
			left outer join Config comment on tab.id = comment.ParentID and comment.Type='table-comment'

			left outer join Config sql_trigger on tab.id = sql_trigger.ParentID and sql_trigger.Type='table-sql-trigger'
			left outer join BigDataValues big_sql_trigger_sql on sql_trigger.DataLinkID = big_sql_trigger_sql.id

			left outer join EIS.dbo.EmpGeneral EmpGeneralCreated ON tab.CreatedByID = EmpGeneralCreated.EGenEmpID
		where tab.id = @ID
	END




	IF @What = 'table-rights' BEGIN
		if (@ID IS NULL OR rtrim(ltrim(@ID)) = '') BEGIN
			raiserror('ID of table is required.', 20, -1) with log
		END
		--declare @ID varchar(50)='698'
		SELECT
			CAST(rights.ID AS VARCHAR(50))[ID],
			rights.type[Right],
			rights.Value[Username],
			RTRIM(EmpGeneralRight.EGenFirstName) + ISNULL(' ('+EmpGeneralRight.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralRight.EGenLastName) AS UserFullname,

			RTRIM(EmpGeneralCreated.EGenFirstName) + ISNULL(' ('+EmpGeneralCreated.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralCreated.EGenLastName) AS CreatedBy,
			rights.CreatedByID,
			FORMAT( rights.CreatedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS CreatedDate
		FROM Config[rights]

			left outer join EIS.dbo.EmpGeneral EmpGeneralCreated ON rights.CreatedByID = EmpGeneralCreated.EGenEmpID
			left outer join EIS.dbo.EmpGeneral EmpGeneralRight ON rights.value = EmpGeneralRight.EGenUserName
		where rights.parentid = @ID
			AND rights.Value <> ''
			AND rights.Value IS Not NULL
			AND (rights.type= 'table-right-viewer'
			OR rights.type= 'table-right-editor'
			OR rights.type= 'table-right-designer')
	END


	IF @What = 'table-group-rights' BEGIN
		if (@ID IS NULL OR rtrim(ltrim(@ID)) = '') BEGIN
			raiserror('ID of table group is required.', 20, -1) with log
		END
		--declare @ID varchar(50)='698'
		SELECT
			CAST(rights.ID AS VARCHAR(50))[ID],
			rights.type[Right],
			rights.Value[Username],
			RTRIM(EmpGeneralRight.EGenFirstName) + ISNULL(' ('+EmpGeneralRight.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralRight.EGenLastName) AS UserFullname,

			RTRIM(EmpGeneralCreated.EGenFirstName) + ISNULL(' ('+EmpGeneralCreated.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralCreated.EGenLastName) AS CreatedBy,
			rights.CreatedByID,
			FORMAT( rights.CreatedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS CreatedDate
		FROM Config[rights]

			left outer join EIS.dbo.EmpGeneral EmpGeneralCreated ON rights.CreatedByID = EmpGeneralCreated.EGenEmpID
			left outer join EIS.dbo.EmpGeneral EmpGeneralRight ON rights.value = EmpGeneralRight.EGenUserName
		where rights.parentid = @ID
			AND rights.Value <> ''
			AND rights.Value IS Not NULL
			AND (rights.type= 'table-group-right-viewer'
			OR rights.type= 'table-group-right-editor'
			OR rights.type= 'table-group-right-designer')
	END



	IF @What = 'data-views' BEGIN
		if (@ID IS NULL OR rtrim(ltrim(@ID)) = '') BEGIN
			raiserror('ID of table is required.', 20, -1) with log
		END
		--declare @ID varchar(50) = '2'
		SELECT
			CAST(sql_data_view.ID AS VARCHAR(50))[ID],
			sql_data_view.value[Name],
			data_view_comment.value[Comment],
			sql_data_view_sql.value[SQLDataView],

			RTRIM(EmpGeneralCreated.EGenFirstName) + ISNULL(' ('+EmpGeneralCreated.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralCreated.EGenLastName) AS CreatedBy,
			sql_data_view.CreatedByID,
			FORMAT( sql_data_view.CreatedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS CreatedDate,

			RTRIM(EmpGeneralModified.EGenFirstName) + ISNULL(' ('+EmpGeneralModified.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralModified.EGenLastName) AS ModifiedBy,
			sql_data_view.ModifiedByID,
			FORMAT( sql_data_view.ModifiedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS ModifiedDate

		FROM Config[tab]
			JOIN Config[tab_rights] ON tab.id = tab_rights.ParentID
				AND tab_rights.Type = 'table-right-viewer'
				AND (tab_rights.Value = @Username OR tab_rights.Value = 'Everyone')
			join Config sql_data_view on tab.id = sql_data_view.ParentID and sql_data_view.Type='sql-data-view'
			left outer join Config[data_view_comment] on sql_data_view.id = data_view_comment.ParentID and data_view_comment.type = 'sql-data-view-comment'

			left outer join BigDataValues sql_data_view_sql on sql_data_view.DataLinkID = sql_data_view_sql.id

			left outer join EIS.dbo.EmpGeneral EmpGeneralCreated ON sql_data_view.CreatedByID = EmpGeneralCreated.EGenEmpID
			left outer join EIS.dbo.EmpGeneral EmpGeneralModified ON sql_data_view.ModifiedByID = EmpGeneralModified.EGenEmpID
		where tab.id = @ID
	END


	IF @What = 'data-view' BEGIN
		if (@ID IS NULL OR rtrim(ltrim(@ID)) = '') BEGIN
			raiserror('ID of table is required.', 20, -1) with log
		END
		--declare @ID varchar(50) = '641'
		SELECT
			CAST(sql_data_view.ID AS VARCHAR(50))[ID],
			sql_data_view.value[Name],
			data_view_comment.value[Comment],
			sql_data_view_sql.value[SQLDataView],

			RTRIM(EmpGeneralCreated.EGenFirstName) + ISNULL(' ('+EmpGeneralCreated.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralCreated.EGenLastName) AS CreatedBy,
			sql_data_view.CreatedByID,
			FORMAT( sql_data_view.CreatedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS CreatedDate,

			RTRIM(EmpGeneralModified.EGenFirstName) + ISNULL(' ('+EmpGeneralModified.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralModified.EGenLastName) AS ModifiedBy,
			sql_data_view.ModifiedByID,
			FORMAT( sql_data_view.ModifiedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS ModifiedDate

		FROM Config[sql_data_view]
			left outer join Config[data_view_comment] on sql_data_view.id = data_view_comment.ParentID and data_view_comment.type = 'sql-data-view-comment'
			left outer join BigDataValues[sql_data_view_sql] on sql_data_view.DataLinkID = sql_data_view_sql.id

			left outer join EIS.dbo.EmpGeneral[EmpGeneralCreated] ON sql_data_view.CreatedByID = EmpGeneralCreated.EGenEmpID
			left outer join EIS.dbo.EmpGeneral[EmpGeneralModified] ON sql_data_view.ModifiedByID = EmpGeneralModified.EGenEmpID
		where sql_data_view.Type='sql-data-view'
			and sql_data_view.id = @ID
	END



	IF @What = 'field-info' OR @What = 'table-field-info' BEGIN
		-- we need id
		if (@ID IS NULL OR rtrim(ltrim(@ID)) = '') BEGIN
			raiserror('ID of field is required.', 20, -1) with log
		END
		-- DECLARE @ID VARCHAR(20) ='648';declare @What varchar(20)='table-field-info';
		SELECT
			isnull(CAST(field.id AS VARCHAR(50)),'')[ID],
			isnull(field.value,'')[Name],
			isnull(CAST(forder.DataLinkID AS VARCHAR(50)),'')[Order],
			isnull(field_type_list_item.Value,'')[TypeSystemName],
			isnull(field_type_list_item_label.Value,'')[Type],
			isnull(CAST(field_type_list_item.ID AS VARCHAR(50)),'')[TypeID],
			isnull(field_tooltip.value,'')[Tooltip],
			isnull(list_item_field_uniqueness.value,'')[Uniqueness],
			isnull(CAST(list_item_field_uniqueness.id AS VARCHAR(50)),'')[UniquenessID],
			isnull(list_item_field_required_type.Value,'')[RequiredType],
			isnull(CAST(list_item_field_required_type.id AS VARCHAR(50)),'')[RequiredTypeID],
			isnull(CAST(regex_for_type.value AS VARCHAR(50)),'')[RegEx],
			isnull(CAST(regex_fail_message_for_type.value AS VARCHAR(50)),'')[RegExFailMessage],
			isnull(CAST(sql_code_lookup.id AS VARCHAR(50)),'')[SQLCodeLookupID],
			--isnull(CAST(sql_multi_code_lookup.id AS VARCHAR(50)),'')[SQLMultiCodeLookupID],
			isnull(CAST(field_multi_selection.Value AS VARCHAR(50)),'')[CodeMultiSelection],
			isnull(CAST(field_choices.value AS VARCHAR(50)),'')[ChoiceList],
			CASE WHEN isnull(CAST(field_hide_on_insert.value AS VARCHAR(50)),'')= 'Y' THEN 'true'
			ELSE 'false' END AS [InsertHidden],
			isnull(CAST(field_hide_on_insert.value AS VARCHAR(50)),'')[InsertHiddenYN],

			RTRIM(EmpGeneralCreated.EGenFirstName) + ISNULL(' ('+EmpGeneralCreated.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralCreated.EGenLastName) AS CreatedBy,
			field.CreatedByID,
			FORMAT( field.CreatedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS CreatedDate,

			RTRIM(EmpGeneralModified.EGenFirstName) + ISNULL(' ('+EmpGeneralModified.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralModified.EGenLastName) AS ModifiedBy,
			field.ModifiedByID,
			FORMAT( field.ModifiedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS ModifiedDate



		FROM Config field
			join Config forder
			on field.id = forder.ParentID and forder.Type = 'field-display-order'

			left outer join Config field_tooltip
			on field.ID = field_tooltip.ParentID and field_tooltip.Type = 'field-tooltip'

			left outer join Config field_multi_selection
			on field.ID = field_multi_selection.ParentID and field_multi_selection.Type = 'field-multi-selection'

			left outer join Config field_choices
			on field.ID = field_choices.ParentID and field_choices.Type = 'field-choices'

			left outer join Config field_uniqueness
			on field.ID = field_uniqueness.ParentID and field_uniqueness.Type = 'field-uniqueness'
			left outer join Config list_item_field_uniqueness
			on field_uniqueness.DataLinkID = list_item_field_uniqueness.id and list_item_field_uniqueness.Type = 'list-item-field-uniqueness'

			left outer join Config field_type
			on field.ID = field_type.ParentID and field_type.Type = 'field-type'

			left outer join Config field_hide_on_insert
			on field.ID = field_hide_on_insert.ParentID and field_hide_on_insert.Type = 'field-hide-on-insert' and field_hide_on_insert.value = 'Y'

			left outer join Config field_type_list_item
			on field_type.DataLinkID= field_type_list_item.id and field_type_list_item.Type = 'list-item-field-type'


			left outer join Config field_type_list_item_label
			on field_type_list_item.id= field_type_list_item_label.parentid and field_type_list_item_label.Type = 'list-item-field-type-label'


			left outer join Config sql_code_lookup
			on field_type_list_item.id= sql_code_lookup.ParentID and sql_code_lookup.Type = 'sql-code-lookup'

			--left outer join Config sql_multi_code_lookup 
			--	on field_type_list_item.id= sql_multi_code_lookup.ParentID and sql_multi_code_lookup.Type = 'sql-multi-code-lookup'

			left outer join Config regex_for_type
			on field_type_list_item.id= regex_for_type.parentid and regex_for_type.Type = 'regex'
			left outer join Config regex_fail_message_for_type
			on regex_for_type.id= regex_fail_message_for_type.parentid and regex_fail_message_for_type.Type = 'regex-fail-message'




			left outer join Config field_required_type
			on field.ID = field_required_type.ParentID and field_required_type.Type = 'field-required-type'
			left outer join Config list_item_field_required_type
			on field_required_type.DataLinkID= list_item_field_required_type.id and list_item_field_required_type.Type = 'list-item-field-required-type'

			left outer join EIS.dbo.EmpGeneral EmpGeneralCreated ON field.CreatedByID = EmpGeneralCreated.EGenEmpID
			left outer join EIS.dbo.EmpGeneral EmpGeneralModified ON field.ModifiedByID = EmpGeneralModified.EGenEmpID

		where field.type = 'field'
			and (@What <> 'field-info' OR field.id = @ID)
			and (@What <> 'table-field-info' OR field.parentid = @ID)
		order by [Order]

	END



	IF @What = 'table-data' or @What = 'data-row' BEGIN
		IF @What = 'table-data' BEGIN
			-- we need id
			IF (@ID IS NULL OR rtrim(ltrim(@ID)) = '') BEGIN
				raiserror('ID of table is required.', 20, -1) with log
			END

			IF NOT EXISTS (SELECT ID
			FROM Config
			WHERE ID = @ID AND Type = 'Table') BEGIN
				raiserror('Table not found.', 20, -1) with log
			END
		END

		IF @What = 'data-row' BEGIN
			-- we need id
			IF (@ID IS NULL OR rtrim(ltrim(@ID)) = '') BEGIN
				raiserror('ID of row is required.', 20, -1) with log
			END
		END

		-- DECLARE @ID VARCHAR(999) ='698'; DECLARE @TopRecordCount VARCHAR(99) = '9999';DECLARE @Search VARCHAR(999) = '';DECLARE @What VARCHAR(50) = 'table-data'
		-- DECLARE @ID VARCHAR(8) ='329'; DECLARE @TopRecordCount VARCHAR(99) = '9999';DECLARE @Search VARCHAR(999) = '';DECLARE @What VARCHAR(50) = 'data-row'
		IF OBJECT_ID('tempdb..#TableData') IS NOT NULL DROP TABLE #TableData
		IF OBJECT_ID('tempdb..#FieldAndValue') IS NOT NULL DROP TABLE #FieldAndValue

		CREATE TABLE #TableData
		(
			system______row_number_for_table_data int,
			system______row_id_number_for_table_data int,
			value VARCHAR(Max),
			field_order int,
			field VARCHAR(3000)
		)

		CREATE TABLE #FieldAndValue
		(
			value VARCHAR(Max),
			field VARCHAR(3000),
			system______row_number_for_table_data int,
			system______row_id_number_for_table_data int,
		)

		INSERT into #TableData
			(system______row_number_for_table_data, system______row_id_number_for_table_data, value, field_order, field)
		SELECT
			table_rows.RowNumber[row_number_for_table_data],
			table_rows.id[row_id_number_for_table_data],

			CASE WHEN LEN(LTRIM(RTRIM(ISNULL(value_lookup.Value, ''))) + LTRIM(RTRIM(ISNULL(big_data_lookup.Value, ''))) + LTRIM(RTRIM(ISNULL(very_big_data_lookup.Value, '')))) > 0 THEN
	CASE WHEN  @What = 'data-row' THEN
		LTRIM(RTRIM(ISNULL(row_field_value.Value, ''))) + LTRIM(RTRIM(ISNULL(big_data.Value, ''))) + LTRIM(RTRIM(ISNULL(very_big_data.Value, ''))) + 
		'[[{{value-lookup-divider}}]]' +
		LTRIM(RTRIM(ISNULL(value_lookup.Value, ''))) + LTRIM(RTRIM(ISNULL(big_data_lookup.Value, ''))) + LTRIM(RTRIM(ISNULL(very_big_data_lookup.Value, '')))
	WHEN @What = 'table-data' THEN
		LTRIM(RTRIM(ISNULL(value_lookup.Value, ''))) + LTRIM(RTRIM(ISNULL(big_data_lookup.Value, ''))) + LTRIM(RTRIM(ISNULL(very_big_data_lookup.Value, '')))
    ELSE
		''
	END
ELSE
	LTRIM(RTRIM(ISNULL(row_field_value.Value, ''))) + LTRIM(RTRIM(ISNULL(big_data.Value, ''))) + LTRIM(RTRIM(ISNULL(very_big_data.Value, '')))
END[value],
			field_display_order.DataLinkID[field_order],
			field.Value[field]

		FROM Config selected_table
			left outer join DataValues table_rows on selected_table.id = table_rows.TableID and table_rows.Type = 'row'

			left outer join DataValues row_field_value on table_rows.id = row_field_value.RowID and row_field_value.Type = 'value'


			left outer join Config field on row_field_value.FieldID= field.id and field.Type = 'field'
			left outer join Config field_type on field.id = field_type.ParentID and field_type.Type = 'field-type'
			left outer join Config list_item_field_type on field_type.DataLinkID = list_item_field_type.id and list_item_field_type.Type = 'list-item-field-type'
			left outer join Config field_type_list_item_label
			on list_item_field_type.id= field_type_list_item_label.parentid and field_type_list_item_label.Type = 'list-item-field-type-label'


			left outer join BigDataValues big_data on row_field_value.bigdatavalueid = big_data.id
			left outer join VeryBigDataValues very_big_data on row_field_value.verybigdatavalueid = very_big_data.id
			left outer join Config field_display_order on field_display_order.ParentID = field.id and field_display_order.Type = 'field-display-order'

			left outer join DataValues value_lookup on row_field_value.RowID = value_lookup.RowID and row_field_value.TableID = value_lookup.TableID and row_field_value.FieldID = value_lookup.FieldID and value_lookup.Type = 'value-lookup'
			left outer join BigDataValues big_data_lookup on value_lookup.bigdatavalueid = big_data_lookup.id
			left outer join VeryBigDataValues very_big_data_lookup on value_lookup.verybigdatavalueid = very_big_data_lookup.id
		where (@What <> 'table-data' or selected_table.id=@ID)
			and (@What <> 'data-row' or table_rows.ID=@ID)
			and selected_table.Type = 'table'
			AND (@Search ='' OR (table_rows.RowNumber in ( select distinct
				table_rows.RowNumber
			from DataValues table_rows
				left outer join DataValues row_field_value on table_rows.id = row_field_value.RowID and row_field_value.Type = 'value'
				left outer join BigDataValues big_data on row_field_value.bigdatavalueid = big_data.id
				left outer join VeryBigDataValues very_big_data on row_field_value.verybigdatavalueid = very_big_data.id
			where (@What <> 'table-data' or table_rows.TableID=@ID)
				and
				table_rows.Type = 'row'
				and (@What <> 'data-row' or table_rows.ID=@ID)
				and (@Search = '' OR ISNULL(row_field_value.Value, '') + ISNULL(big_data.Value, '') + ISNULL(very_big_data.Value, '') like '%' + @Search + '%'))
))
		order by field_display_order.DataLinkID, row_number_for_table_data

		DECLARE @Columns VARCHAR(MAX) = ''
		DECLARE @Query NVARCHAR(MAX) = ''

		SELECT @Columns = STUFF((SELECT ',' + QUOTENAME(field)
			FROM #TableData c
			group by field
			order by max(field_order)
			FOR XML PATH(''), TYPE
            ).value('.', 'VARCHAR(MAX)') 
        ,1,1,'')

		INSERT into #FieldAndValue
			(value, field, system______row_number_for_table_data, system______row_id_number_for_table_data)
		SELECT value, field, system______row_number_for_table_data, system______row_id_number_for_table_data
		from #TableData
		where field is not null

		SET @Query = 'SELECT TOP(' + CAST(@TopRecordCount AS VARCHAR(8)) + ') * from #FieldAndValue x pivot ( max( value ) for field in (' + @Columns + ') ) p '

		EXECUTE(@Query)
		DROP TABLE #TableData
		DROP TABLE #FieldAndValue
	END



	IF @What = 'table-fields' BEGIN
		-- we need id
		if (@ID IS NULL OR rtrim(ltrim(@ID)) = '') BEGIN
			raiserror('ID of table is required.', 20, -1) with log
		END

		-- DECLARE @ID VARCHAR(20) ='698'
		SELECT
			field_display_order.DataLinkID[system______display_order_for_field],
			CAST(field.id AS VARCHAR(50))[system______id_number_for_field],
			field.Value[Name],
			isnull(CAST(field_display_order.DataLinkID AS VARCHAR(50)),'') +
			', ' + field_type_list_item_label.value +
			ISNULL(', ' + list_item_field_required_type.Value,'') +
			ISNULL(', ' + list_item_field_uniqueness.Value,'') +
			ISNULL(', tooltip: ' + nullif(field_tooltip.Value,''),'')[Field Specification]

		FROM Config selected_table
			left outer join Config field on selected_table.id= field.ParentID and field.Type = 'field'
			left outer join Config field_display_order on field_display_order.ParentID = field.id and field_display_order.Type = 'field-display-order'
			left outer join Config field_type_id on field.id = field_type_id.ParentID and field_type_id.Type = 'field-type'
			left outer join Config field_type on field_type_id.DataLinkID = field_type.id and field_type.Type = 'list-item-field-type'

			left outer join Config field_type_list_item_label
			on field_type.id= field_type_list_item_label.parentid and field_type_list_item_label.Type = 'list-item-field-type-label'

			left outer join Config field_tooltip
			on field.ID = field_tooltip.ParentID and field_tooltip.Type = 'field-tooltip'


			left outer join Config field_uniqueness
			on field.ID = field_uniqueness.ParentID and field_uniqueness.Type = 'field-uniqueness'
			left outer join Config list_item_field_uniqueness
			on field_uniqueness.DataLinkID = list_item_field_uniqueness.id and list_item_field_uniqueness.Type = 'list-item-field-uniqueness'

			left outer join Config field_required_type
			on field.ID = field_required_type.ParentID and field_required_type.Type = 'field-required-type'
			left outer join Config list_item_field_required_type
			on field_required_type.DataLinkID= list_item_field_required_type.id and list_item_field_required_type.Type = 'list-item-field-required-type'


		where selected_table.id = @ID and selected_table.Type = 'table'
		order by system______display_order_for_field
	END



	IF @What = 'field-types' BEGIN
		SELECT
			CAST(type_system_name.ID AS VARCHAR(50))[ID],
			field_type_list_item_label.Value,
			type_system_name.value[SystemName],
			sql_code_lookup.ID[SQLLookupID]
		from Config type_system_name
			left outer join Config field_type_list_item_label
			on type_system_name.id= field_type_list_item_label.parentid and field_type_list_item_label.Type = 'list-item-field-type-label'
			left outer join Config sql_code_lookup
			on type_system_name.id= sql_code_lookup.ParentID and sql_code_lookup.Type = 'sql-code-lookup'
		where type_system_name.type ='list-item-field-type'
		order by field_type_list_item_label.value
	END


	IF @What = 'field-uniqueness-types' BEGIN
		select
			CAST(ID AS VARCHAR(50))[ID],
			Value
		from Config
		where type ='list-item-field-uniqueness'
		order by value
	END


	IF @What = 'field-required-types' BEGIN
		select
			CAST(ID AS VARCHAR(50))[ID],
			Value
		from Config
		where type ='list-item-field-required-type'
		order by value
	END

	IF @What = 'email-trigger-times' BEGIN
		select
			CAST(ID AS VARCHAR(50))[ID],
			Value
		from Config
		where type ='list-item-email-trigger-times'
		order by case when id = 516 then 0 else 1 end,
		Value
	END



	IF @What = 'sql-code-lookup-list' BEGIN
		--declare @ID varchar(50) = '685'
		DECLARE @SQLCodeLookupString VARCHAR(3000);

		IF @ID = '----SelectUserRights----' BEGIN
			SET @SQLCodeLookupString = (select top 1
				Value
			from Config
			where type = 'sql-username-lookup')
		END
	ELSE BEGIN
			SET @SQLCodeLookupString = (select Value
			from Config
			where id = @ID and type ='sql-code-lookup')
		END

		SET @SQLCodeLookupString = REPLACE( @SQLCodeLookupString, '{{TopRecordCount}}', @TopRecordCount);
		SET @SQLCodeLookupString = REPLACE( @SQLCodeLookupString, '{{SearchString}}', @Search);

		EXEC(@SQLCodeLookupString)
	END


	--IF @What = 'sql-multi-code-lookup-list'
	--BEGIN
	----declare @ID varchar(50) = '685'
	--DECLARE @SQLMultiCodeLookupString VARCHAR(3000) = (select Value from Config where id = @ID and  type ='sql-multi-code-lookup')

	--SET @SQLMultiCodeLookupString = REPLACE( @SQLMultiCodeLookupString, '{{TopRecordCount}}', @TopRecordCount);
	--SET @SQLMultiCodeLookupString = REPLACE( @SQLMultiCodeLookupString, '{{SearchString}}', @Search);

	--EXEC(@SQLMultiCodeLookupString)
	--END





	IF @What = 'check-email-trigger-subscription' BEGIN
		--declare @ID varchar(50) = '147', @Search Varchar(50) = 'stracner'
		select top 1
			ISNULL(CAST(ParentID AS VARCHAR(50)),'')[ID],
			ISNULL(CAST(DataLinkID AS VARCHAR(50)),'')[Value]
		from Config
		where type ='sql-report-subscription' and CreatedBy=@Search and ParentID=@id
	END




	IF @What = 'admin-config-records' BEGIN
		select TOP(@TopRecordCount)
			ISNULL(CAST(mainitem.[ID] AS VARCHAR(50)),'')[ID], ISNULL(CAST(mainitem.[ParentID] AS VARCHAR(50)),'') + 
	  ISNULL(CAST(' (Type=' + parentitem.type + ')' AS VARCHAR(50)),'') + 
	  ISNULL(CAST(' (Value=' + parentitem.value + ')' AS VARCHAR(50)),'')
	  [ParentID]
      , mainitem.[Type]
      , mainitem.[Value],
			CASE
	  WHEN mainitem.[Type]='field-display-order' THEN
	     'ORDER: ' + ISNULL(CAST(mainitem.[DataLinkID] AS VARCHAR(50)),'')
	  ELSE
		ISNULL(CAST(mainitem.[DataLinkID] AS VARCHAR(50)),'') + 
		ISNULL(CAST(' (Type=' + linked_data.type + ')' AS VARCHAR(50)),'') + 
		ISNULL(CAST(' (Value=' + linked_data.value + ')' AS VARCHAR(50)),'')
	  END[DataLinkID]
      , mainitem.[CreatedBy]
      , ISNULL(CAST(mainitem.[CreatedByID] AS VARCHAR(50)),'')[CreatedByID]
      , mainitem.[CreatedDate]
      , mainitem.[ModifiedBy]
      , ISNULL(CAST(mainitem.[ModifiedByID] AS VARCHAR(50)),'')[DeletedByID]
      , mainitem.[ModifiedDate]
		from Config mainitem
			left outer join Config parentitem on mainitem.ParentID= parentitem.id
			left outer join Config linked_data on mainitem.datalinkid = linked_data.id

		where (@Search = '' or mainitem.value like '%' + @Search + '%' or mainitem.type like '%' + @Search + '%' or mainitem.id like '%' + @Search + '%' or mainitem.ParentID like '%' + @Search + '%' or mainitem.DataLinkID like '%' + @Search + '%')
		order by type, value
	END


	IF @What = 'admin-config-record' BEGIN
		SELECT
			ISNULL(CAST([ID] AS VARCHAR(50)),'')[ID]
      , ISNULL(CAST([ParentID] AS VARCHAR(50)),'')[ParentID]
      , [Type]
      , [Value]
      , ISNULL(CAST([DataLinkID] AS VARCHAR(50)),'')[DataLinkID]
      ,

			RTRIM(EmpGeneralCreated.EGenFirstName) + ISNULL(' ('+EmpGeneralCreated.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralCreated.EGenLastName) AS CreatedBy,
			ISNULL(CAST(Config.CreatedByID AS VARCHAR(50)),'')[CreatedByID],
			FORMAT( Config.CreatedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS CreatedDate,

			RTRIM(EmpGeneralModified.EGenFirstName) + ISNULL(' ('+EmpGeneralModified.EGenPreferredName+')','') + ' ' + RTRIM(EmpGeneralModified.EGenLastName) AS CreatedBy,
			ISNULL(CAST(Config.ModifiedByID AS VARCHAR(50)),'')[DeletedByID],
			FORMAT( Config.ModifiedDate, 'MM/dd/yyyy hh:mm tt', 'en-US' ) AS DeletedDate

		FROM Config
			LEFT OUTER JOIN EIS.dbo.EmpGeneral EmpGeneralCreated ON Config.CreatedByID = EmpGeneralCreated.EGenEmpID
			LEFT OUTER JOIN EIS.dbo.EmpGeneral EmpGeneralModified ON Config.ModifiedByID = EmpGeneralModified.EGenEmpID
		WHERE ID=@ID
	END

	IF @What = 'table-id-by-name' BEGIN
		--declare @Search varchar(99)='staa'
		SELECT ISNULL(CAST([ID] AS VARCHAR(50)),'')[ID]
		FROM Config
		WHERE Value=@Search and Type='table'
	END

	IF @What = 'table-group-id-by-name' BEGIN
		--declare @Search varchar(99)='ITS Tables'
		SELECT ISNULL(CAST([ID] AS VARCHAR(50)),'')[ID]
		FROM Config
		WHERE Value=@Search and Type='table-group'
	END

	IF @What = 'report-id-by-name' BEGIN
		SELECT ISNULL(CAST([ID] AS VARCHAR(50)),'')[ID]
		FROM Config sql_data_view
		where sql_data_view.ID = @ID and sql_data_view.Type = 'sql-data-view'
	END

	IF @What = 'user-table-rights' BEGIN
		DECLARE @TableGroupIDForGroupRights VARCHAR(255) = (SELECT ParentID
		FROM Config
		WHERE ID = @ID AND Type = 'table');

							SELECT ID, type[Value]
			FROM Config
			WHERE (type = 'table-right-editor'
				OR type = 'table-right-viewer'
				OR type = 'table-right-designer')
				AND (Value = @Username OR Value = 'Everyone')
				AND (ParentID = @ID)
		UNION
			SELECT ID, type[Value]
			FROM Config
			WHERE (TYPE = 'table-group-right-editor'
				OR type = 'table-group-right-viewer'
				OR type = 'table-group-right-designer')
				AND
				(Value = @Username OR Value = 'Everyone')
				AND
				(ParentID = @TableGroupIDForGroupRights)
		UNION
			SELECT ID, type[Value]
			FROM Config
			WHERE Type = 'system-admin'
				AND Value = @Username
	END

	IF @What = 'user-table-group-rights' BEGIN
					SELECT ID, type[Value]
			FROM Config
			WHERE (TYPE = 'table-group-right-editor'
				OR TYPE = 'table-group-right-viewer'
				OR TYPE = 'table-group-right-designer')
				AND
				(Value = @Username OR Value = 'Everyone')
				AND
				(ParentID = @ID)
		UNION
			SELECT ID, type[Value]
			FROM Config
			WHERE Type = 'system-admin'
				AND Value = @Username
	END
END