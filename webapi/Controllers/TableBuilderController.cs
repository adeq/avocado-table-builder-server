﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Data;
using System.Data.Odbc;

namespace WEBAPI_JWT_Authentication.Controllers {
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    public class TableBuilderController : ApiController {
        [HttpPost]
        public DataTable Post([FromBody]IDictionary<string, string> oPost) {
            DataTable dtData = new DataTable();
            try {

                if (Security.IsInternalADEQIP(this.User) == false) {
                    throw new Exception("External address.");
                }

                if (oPost == null) throw new Exception("Missing parameters.");
                if (oPost.Count < 1) throw new Exception("Missing parameters.");

                if (oPost["What"] == "admin-config-records" ||
                    oPost["What"] == "admin-config-record" ||
                    oPost["What"] == "update-admin-config-record" ||
                    oPost["What"] == "insert-admin-config-record" ||
                    oPost["What"] == "delete-admin-config-record") {
                    if (!Security.CheckAuthorization(this.User.Identity.Name.ToLower(), "TableBuilder_AdminPriv".ToLower())) {
                        throw new Exception("Missing admin permission.");
                    }
                }

                if (oPost.Keys.Contains("IsUpdate") && oPost["IsUpdate"] == "True") {
                    return GetUpdateDatatable(oPost["What"], 
                        oPost["Action"], 
                        oPost["ID"], 
                        oPost["Value"], 
                        oPost["CreatedBy"], 
                        oPost["CreatedByID"]);
                } else {
                    return GetDatatable(oPost["What"], 
                        oPost["ID"], 
                        oPost["Search"], 
                        int.Parse(oPost["Page"]), 
                        int.Parse(oPost["RecordsPerPage"]));
                }
            } catch (Exception e) {
                DataTable dtErrorData = new DataTable();
                dtErrorData.Clear();
                dtErrorData.Columns.Add("WebAPIMessageType");
                dtErrorData.Columns.Add("WebAPIMessage");
                DataRow oDataRow = dtErrorData.NewRow();
                oDataRow["WebAPIMessageType"] = "Error";
                oDataRow["WebAPIMessage"] = e.Message;
                dtErrorData.Rows.Add(oDataRow);

                Security.Log(this.User, "Error " + e.Message);
                return dtErrorData;
            }
        }

        private DataTable GetDatatable(string What, string ID, string Search, int Page, int RecordsPerPage) {
            DataTable dtData = new DataTable();
            try {
                string strConnectionString = System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Reader"].ToString();
                if (string.IsNullOrEmpty(strConnectionString)) {
                    throw new Exception("Missing connection string.");
                }
                if (What == "execute-data-view" || What == "sql-code-lookup-list") {
                    strConnectionString = System.Configuration.ConfigurationManager.AppSettings["connection-string-UserSQLDB_Reader"].ToString();
                    if (string.IsNullOrEmpty(strConnectionString)) {
                        throw new Exception("Missing connection string.");
                    }
                }

                What = Security.ProtectInjection(What);
                ID = Security.ProtectInjection(ID);
                Search = Security.ProtectInjectionWithLikeClause(Search);

                OdbcCommand cmdOdbcCommand = new OdbcCommand();
                OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
                OdbcConnection connConnection;
                connConnection = new OdbcConnection(strConnectionString);
                cmdOdbcCommand.CommandType = CommandType.Text;
                cmdOdbcCommand.CommandText = "EXEC up_Get @What='" + What + 
                    "', @ID='" + ID + 
                    "', @Search ='" + Search + 
                    "', @Username = '" + this.User.Identity.Name.ToLower() + "' ";

                if (Page != 0) {
                    cmdOdbcCommand.CommandText += ", @TopRecordCount=" + (((Page + 1) * RecordsPerPage).ToString());
                }
                cmdOdbcCommand.Connection = connConnection;
                daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
                daSqlDataAdapter.Fill(dtData);
                if (dtData.Rows.Count == 0) throw new Exception("The source contains no DataRows.");

                if (RecordsPerPage != 0) {
                    return dtData.AsEnumerable()
                        .Skip(Page * RecordsPerPage)
                        .Take(RecordsPerPage).CopyToDataTable();
                } else {
                    return dtData;
                }
            } catch (Exception e) {
                DataTable dtErrorData = new DataTable();
                dtErrorData.Clear();
                dtErrorData.Columns.Add("WebAPIMessageType");
                dtErrorData.Columns.Add("WebAPIMessage");
                DataRow oDataRow = dtErrorData.NewRow();
                oDataRow["WebAPIMessageType"] = "Error";
                oDataRow["WebAPIMessage"] = e.Message;
                dtErrorData.Rows.Add(oDataRow);

                Security.Log(this.User, "Error " + e.Message);
                return dtErrorData;
            }
        }

        private DataTable GetUpdateDatatable(string What, string Action, string ID, string Value, string CreatedBy, string CreatedByID) {
            DataTable dtData = new DataTable();
            try {
                string strConnectionString = System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString();
                if (string.IsNullOrEmpty(strConnectionString)) {
                    throw new Exception("Missing connection string.");
                }

                DataTable dtAuthyCheckReverify = Security.AuthyUpdateSecurityCheck(this.User);
                if (dtAuthyCheckReverify != null) return dtAuthyCheckReverify;

                What = Security.ProtectInjection(What);
                Action = Security.ProtectInjection(Action);
                ID = Security.ProtectInjection(ID);
                Value = Security.ProtectInjection(Value);
                CreatedBy = Security.ProtectInjection(CreatedBy);
                CreatedByID = Security.ProtectInjection(CreatedByID);

                OdbcCommand cmdOdbcCommand = new OdbcCommand();
                OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
                OdbcConnection connConnection;
                connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
                cmdOdbcCommand.CommandType = CommandType.Text;
                cmdOdbcCommand.CommandText = "EXEC up_Update " +
                    "@What='" + What +
                    "', @Action='" + Action +
                    "', @ID='" + ID +
                    "', @Value ='" + Value +
                    "', @CreatedBy ='" + this.User.Identity.Name.ToLower() +
                    "', @CreatedByID ='" + CreatedByID + "' ";
                cmdOdbcCommand.Connection = connConnection;
                daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
                daSqlDataAdapter.Fill(dtData);

                return dtData;
            } catch (Exception e) {
                DataTable dtErrorData = new DataTable();
                dtErrorData.Clear();
                dtErrorData.Columns.Add("WebAPIMessageType");
                dtErrorData.Columns.Add("WebAPIMessage");
                DataRow oDataRow = dtErrorData.NewRow();
                oDataRow["WebAPIMessageType"] = "Error";
                oDataRow["WebAPIMessage"] = e.Message;
                dtErrorData.Rows.Add(oDataRow);

                Security.Log(this.User, "Error " + e.Message);
                return dtErrorData;
            }
        }

    //    private DataTable Search(string Search, int Page, int RecordsPerPage) {
    //        DataTable dtData = new DataTable();
    //        try {
    //            bool booIsAdmin = false;
    //            if (Security.CheckAuthorization(this.User.Identity.Name.ToLower(), "TableBuilder_AdminPriv")) {
    //                booIsAdmin = true;
    //            }

    //            Search = Security.ProtectInjectionWithLikeClause(Search);

    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "EXEC up_GetTableBuilderList @SearchContains='" + Search + "' ";
    //            if (booIsAdmin) {
    //                cmdOdbcCommand.CommandText += ", @Username='ALL'";
    //            } else {
    //                cmdOdbcCommand.CommandText += ", @Username='" + this.User.Identity.Name.ToLower() + "'";
    //            }
    //            if (Page != 0) {
    //                cmdOdbcCommand.CommandText += ", @TopRecordCount=" + (((Page + 1) * RecordsPerPage).ToString());
    //            }
    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);
    //            if (dtData.Rows.Count == 0) throw new Exception("The source contains no DataRows.");

    //            return dtData.AsEnumerable()
    //                .Skip(Page * RecordsPerPage)
    //                .Take(RecordsPerPage).CopyToDataTable();
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);

    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }


    //    private DataTable GetTableBuilderRecord(string ID) {
    //        DataTable dtData = new DataTable();
    //        try {

    //            ID = Security.ProtectInjection(ID);

    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Reader"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "EXEC up_GetTableBuilderRecord @ID = " + ID + "";
    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);

    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }

    //    private DataTable GetTableBuilderRecordBySectionTitle(string Path, string SectionTitle) {
    //        DataTable dtData = new DataTable();
    //        try {
    //            Path = Security.ProtectInjection(Path);
    //            SectionTitle = Security.ProtectInjection(SectionTitle);

    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "EXEC up_GetTableBuilderRecordFromSectionTitle @Path = '" + Path + "', @SectionTitle = '" + SectionTitle + "'";
    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);
    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }

    //    private DataTable Update(string RecordAction, string ID, string Path, string SectionTitle, string Html, string CreatedBy, string CreatedByID, string CreatedDate, string ModifiedBy, string ModifiedByID, string ModifiedDate) {
    //        try {
    //            DataTable dtAuthyCheckReverify = Security.AuthyUpdateSecurityCheck(this.User);
    //            if (dtAuthyCheckReverify != null) return dtAuthyCheckReverify;

    //            ID = Security.ProtectInjection(ID);
    //            Path = Security.ProtectInjection(Path);
    //            SectionTitle = Security.ProtectInjection(SectionTitle);
    //            Html = Security.ProtectInjection(Html);
    //            CreatedBy = Security.ProtectInjection(CreatedBy);
    //            CreatedByID = Security.ProtectInjection(CreatedByID);
    //            CreatedDate = Security.ProtectInjection(CreatedDate);
    //            ModifiedBy = Security.ProtectInjection(ModifiedBy);
    //            ModifiedByID = Security.ProtectInjection(ModifiedByID);
    //            ModifiedDate = Security.ProtectInjection(ModifiedDate);

    //            DataTable dtData = new DataTable();
    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "EXEC up_UpdateTableBuilder " +
    //                                        "@RecordAction = '" + RecordAction + "', " +
    //                                        "@ID = '" + ID + "', " +
    //                                        "@Path = '" + Path + "', " +
    //                                        "@SectionTitle = '" + SectionTitle + "', " +
    //                                        "@Html = '" + Html + "', " +
    //                                        "@CreatedBy = '" + CreatedBy + "', " +
    //                                        "@CreatedByID = '" + CreatedByID + "', " +
    //                                        "@ModifiedBy = '" + ModifiedBy + "', " +
    //                                        "@ModifiedByID = '" + ModifiedByID + "'";

    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            Security.Log(this.User, "Update. Record action: " + RecordAction + " ID: " + ID);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);
    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }

    //    private DataTable GetUserInfo(string UserName) {
    //        DataTable dtData = new DataTable();
    //        try {
    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-ProgramSettings_Reader"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            //cmdOdbcCommand.CommandText = " EXECUTE up_GetUserInfo_TableBuilder @UserName = '" + TableBuilderUserIDUserName.UserName + "' ";
    //            cmdOdbcCommand.CommandText = " EXECUTE up_GetUserInfo @UserName = '" + UserName + "' ";
    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);
    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }

    //    private DataTable GetHtmlHistoryList(string ID) {
    //        DataTable dtData = new DataTable();
    //        try {
    //            ID = Security.ProtectInjectionWithLikeClause(ID);

    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "exec up_GetTableBuilderHistoryList @ID ='" + ID + "' ";
    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);
    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }


    //    private DataTable GetHtmlHistoryItem(string ID) {
    //        DataTable dtData = new DataTable();
    //        try {
    //            ID = Security.ProtectInjectionWithLikeClause(ID);

    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "EXEC up_GetTableBuilderHistoryRecord @ID ='" + ID + "' ";
    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);
    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }

    //    private DataTable FileSearch(string Search, int Page, int RecordsPerPage) {
    //        DataTable dtData = new DataTable();
    //        try {
    //            bool booIsAdmin = false;
    //            if (Security.CheckAuthorization(this.User.Identity.Name.ToLower(), "TableBuilder_AdminPriv")) {
    //                booIsAdmin = true;
    //            }

    //            Search = Security.ProtectInjectionWithLikeClause(Search);

    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "EXEC up_GetWebsiteFileListsList @SearchContains='" + Search + "' ";
    //            if (booIsAdmin) {
    //                cmdOdbcCommand.CommandText += ", @Username='ALL'";
    //            } else {
    //                cmdOdbcCommand.CommandText += ", @Username='" + this.User.Identity.Name.ToLower() + "'";
    //            }
    //            if (Page != 0) {
    //                cmdOdbcCommand.CommandText += ", @TopRecordCount=" + (((Page + 1) * RecordsPerPage).ToString());
    //            }
    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);
    //            if (dtData.Rows.Count == 0) throw new Exception("The source contains no DataRows.");
    //            return dtData.AsEnumerable()
    //                .Skip(Page * RecordsPerPage)
    //                .Take(RecordsPerPage).CopyToDataTable();
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);

    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }

    //    private DataTable GetTableBuilderFileRecord(string ID) {
    //        DataTable dtData = new DataTable();
    //        try {
    //            bool booIsAdmin = false;
    //            if (Security.CheckAuthorization(this.User.Identity.Name.ToLower(), "TableBuilder_AdminPriv")) {
    //                booIsAdmin = true;
    //            }

    //            ID = Security.ProtectInjection(ID);

    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "EXEC up_GetWebsiteFileListRecord @ID = " + ID + "";
    //            if (booIsAdmin) {
    //                cmdOdbcCommand.CommandText += ", @Username='ALL'";
    //            } else {
    //                cmdOdbcCommand.CommandText += ", @Username='" + this.User.Identity.Name.ToLower() + "'";
    //            }
    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);

    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }

    //    private DataTable GetWebsiteFileRecordBySectionTitle(string Path, string SectionTitle) {
    //        DataTable dtData = new DataTable();
    //        try {
    //            Path = Security.ProtectInjection(Path);
    //            SectionTitle = Security.ProtectInjection(SectionTitle);

    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "EXEC up_GetWebsiteFileListRecordBySectionTitle @Path = '" + Path + "', @SectionTitle = '" + SectionTitle + "'";

    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);

    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }

    //    private DataTable GetEmployeeList() {
    //        DataTable dtData = new DataTable();
    //        try {
    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-ProgramSettings_Reader"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "EXEC dbo.up_GetEmployeeUsernameList ";
    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);
    //            return dtErrorData;
    //        }
    //    }




    //    private DataTable GetFileHistoryList(string ID) {
    //        DataTable dtData = new DataTable();
    //        try {
    //            ID = Security.ProtectInjectionWithLikeClause(ID);

    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "exec up_GetWebsiteFileListHistoryList @ID ='" + ID + "' ";
    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);
    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }


    //    private DataTable GetFileHistoryItem(string ID) {
    //        DataTable dtData = new DataTable();
    //        try {
    //            ID = Security.ProtectInjectionWithLikeClause(ID);

    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "EXEC up_GetWebsiteFileListHistoryRecord @ID ='" + ID + "' ";
    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);
    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }


    //    private DataTable FileListUpdate(string RecordAction, string ID, string Path,
    //        string SectionTitle, string FilePaths, string FileTitles, string FileType,
    //        string FileCreatedBy, string PrivateComments, string Editors, string TestSiteOnlyYN,
    //        string CreatedBy, string CreatedByID, string CreatedDate,
    //        string ModifiedBy, string ModifiedByID, string ModifiedDate) {
    //        try {
    //            if (Path == null || Path.Trim().ToLower() != "https://www.adeq.state.ar.us/intranet/home/buy-sell-trade-free") {
    //                if (!Security.CheckAuthorization(this.User.Identity.Name.ToLower(), "websitefilelist_updatepriv") &&
    //                    !Security.CheckAuthorization(this.User.Identity.Name.ToLower(), "TableBuilder_AdminPriv")) {
    //                    throw new Exception("Error: Rights were not found for the user " + this.User.Identity.Name.ToLower() + ".");
    //                }
    //            }

    //            DataTable dtAuthyCheckReverify = Security.AuthyUpdateSecurityCheck(this.User);
    //            if (dtAuthyCheckReverify != null) return dtAuthyCheckReverify;

    //            bool booIsAdmin = false;
    //            if (Security.CheckAuthorization(this.User.Identity.Name.ToLower(), "TableBuilder_AdminPriv")) {
    //                booIsAdmin = true;
    //            }

    //            ID = Security.ProtectInjection(ID);
    //            Path = Security.ProtectInjection(Path);
    //            SectionTitle = Security.ProtectInjection(SectionTitle);
    //            FilePaths = Security.ProtectInjection(FilePaths);
    //            FileTitles = Security.ProtectInjection(FileTitles);
    //            FileType = Security.ProtectInjection(FileType);
    //            FileCreatedBy = Security.ProtectInjection(FileCreatedBy);
    //            PrivateComments = Security.ProtectInjection(PrivateComments);
    //            Editors = Security.ProtectInjection(Editors);
    //            TestSiteOnlyYN = Security.ProtectInjection(TestSiteOnlyYN);
    //            CreatedBy = Security.ProtectInjection(CreatedBy);
    //            CreatedByID = Security.ProtectInjection(CreatedByID);
    //            CreatedDate = Security.ProtectInjection(CreatedDate);
    //            ModifiedBy = Security.ProtectInjection(ModifiedBy);
    //            ModifiedByID = Security.ProtectInjection(ModifiedByID);
    //            ModifiedDate = Security.ProtectInjection(ModifiedDate);

    //            DataTable dtData = new DataTable();
    //            OdbcCommand cmdOdbcCommand = new OdbcCommand();
    //            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
    //            OdbcConnection connConnection;
    //            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Updater"].ToString());
    //            cmdOdbcCommand.CommandType = CommandType.Text;
    //            cmdOdbcCommand.CommandText = "EXEC up_UpdateWebsiteFileList " +
    //                                        "@RecordAction = '" + RecordAction + "', " +
    //                                        "@ID = '" + ID + "', " +
    //                                        "@Path = '" + Path + "', " +
    //                                        "@SectionTitle = '" + SectionTitle + "', " +
    //                                        "@FilePaths = '" + FilePaths + "', " +
    //                                        "@FileTitles = '" + FileTitles + "', " +
    //                                        "@FileType = '" + FileType + "', " +
    //                                        "@FileCreatedBy = '" + FileCreatedBy + "', " +
    //                                        "@PrivateComments = '" + PrivateComments + "', " +
    //                                        "@Editors = '" + Editors + "', " +
    //                                        "@TestSiteOnlyYN = '" + TestSiteOnlyYN + "', " +
    //                                        "@CreatedBy = '" + CreatedBy + "', " +
    //                                        "@CreatedByID = '" + CreatedByID + "', " +
    //                                        "@ModifiedBy = '" + ModifiedBy + "', " +
    //                                        "@ModifiedByID = '" + ModifiedByID + "'";

    //            if (booIsAdmin) {
    //                cmdOdbcCommand.CommandText += ", @Username='ALL'";
    //            } else {
    //                cmdOdbcCommand.CommandText += ", @Username='" + this.User.Identity.Name.ToLower() + "'";
    //            }

    //            cmdOdbcCommand.Connection = connConnection;
    //            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
    //            daSqlDataAdapter.Fill(dtData);

    //            Security.Log(this.User, "Update. Record action: " + RecordAction + " ID: " + ID);

    //            return dtData;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);
    //            Security.Log(this.User, "Error " + e.Message);
    //            return dtErrorData;
    //        }
    //    }

    //    [HttpPost]
    //    public DataTable FileDataPost() {
    //        string strFTPUsername = "admin";
    //        string strFTPPass = "MAXTECH";

    //        string strPath = "";
    //        string strAction = "";
    //        string strFilename = "";
    //        string strTestSiteOnlyYN = "";
    //        bool booIntranetFile = false;
    //        string strDebugValues = "";

    //        try {
    //            if (!Security.IsInternalADEQIP(this.User)) {
    //                throw new Exception("Error: External IP.");
    //            }

    //            foreach (string key in System.Web.HttpContext.Current.Request.Form.AllKeys) {
    //                if (key == "Action") {
    //                    strAction = System.Web.HttpContext.Current.Request.Form[key];
    //                } else if (key == "Path") {
    //                    strPath = System.Web.HttpContext.Current.Request.Form[key];
    //                } else if (key == "Filename") {
    //                    strFilename = System.Web.HttpContext.Current.Request.Form[key];
    //                } else if (key == "TestSiteOnlyYN") {
    //                    strTestSiteOnlyYN = System.Web.HttpContext.Current.Request.Form[key];
    //                }
    //                strDebugValues = strDebugValues + " Key: " + key + " = " + System.Web.HttpContext.Current.Request.Form[key] + ", ";
    //            }

    //            if (strPath.Trim().ToLower() != "https://www.adeq.state.ar.us/intranet/home/buy-sell-trade-free") {
    //                if (!Security.CheckAuthorization(this.User.Identity.Name.ToLower(), "websitefilelist_updatepriv") &&
    //                    !Security.CheckAuthorization(this.User.Identity.Name.ToLower(), "TableBuilder_AdminPriv")) {
    //                    throw new Exception("Error: Rights were not found for the user " + this.User.Identity.Name.ToLower() + ".");
    //                }
    //            }

    //            strPath = strPath.ToLower();
    //            strPath = strPath.Replace(@"\", "/");
    //            if (strPath.StartsWith("https://www.adeq.state.ar.us/intranet/home/")) {
    //                booIntranetFile = true;
    //                strPath = strPath.Replace("https://www.adeq.state.ar.us/intranet/home/", "/");
    //                strPath = strPath.Replace("https://www.adeq.state.ar.us/intranet/home", "/");
    //            }

    //            if (strPath.StartsWith("https://www.adeq.state.ar.us")) {
    //                strPath = strPath.Replace("https://www.adeq.state.ar.us/", "/");
    //                strPath = strPath.Replace("https://www.adeq.state.ar.us", "/");
    //            }

    //            if (strPath.StartsWith("https://www.adeq.state.ar.us")) {
    //                strPath = strPath.Replace("https://www.adeq.state.ar.us/", "/");
    //                strPath = strPath.Replace("https://www.adeq.state.ar.us", "/");
    //            }
    //            if (strPath.StartsWith("https://www.adeq.state.ar.us")) {
    //                strPath = strPath.Replace("https://www.adeq.state.ar.us/", "/");
    //                strPath = strPath.Replace("https://www.adeq.state.ar.us", "/");
    //            }

    //            if (System.Web.HttpContext.Current.Request.Files.Count < 1 && strAction == "Save") {
    //                throw new Exception("No file was sent to server.");
    //            }
    //            System.Web.HttpPostedFile oHttpPostedFile = null;
    //            if (System.Web.HttpContext.Current.Request.Files.Count > 0)
    //                oHttpPostedFile = System.Web.HttpContext.Current.Request.Files[0];

    //            // make sure the path starts and ends with /
    //            if (!strPath.EndsWith("/")) strPath = strPath + "/";
    //            if (!strPath.StartsWith("/")) strPath = "/" + strPath;

    //            if (strPath.EndsWith(".aspx/")) strPath = strPath.Replace(".aspx/", "/");

    //            if (booIntranetFile) {
    //                string strBasePath = @"E:\Intranet\downloads";
    //                strPath = strPath.Replace("/", "\\");
    //                string strNewFilePath = strBasePath + strPath + strFilename;
    //                // Save Delete
    //                if (strAction == "Save") {
    //                    if (System.IO.File.Exists(strNewFilePath)) {
    //                        throw new Exception("Error the file " + strFilename + " already exists on the server.");
    //                    }
    //                    oHttpPostedFile.SaveAs(strNewFilePath);
    //                } else if (strAction == "Delete") {
    //                    if (!System.IO.File.Exists(strNewFilePath)) {
    //                        throw new Exception("Error the file " + strFilename + " does not exists on the server.");
    //                    }

    //                    string strMakeIntranetFileUnique = System.Text.RegularExpressions.Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), "[/+=]", "").Substring(1, 6);
    //                    System.IO.File.Move(strNewFilePath, @"E:\Intranet\downloads-backup" + strPath + strMakeIntranetFileUnique + strFilename);
    //                } else {
    //                    throw new Exception("Invalid action.");
    //                }
    //                return null;
    //            }

    //            string strProductionPath = "ftp://ftp.adpce.ad/files" + strPath + strFilename;
    //            string strTestPath = "ftp://ftpt.adpce.ad/files" + strPath + strFilename;

    //            string strMakeUnique = System.Text.RegularExpressions.Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), "[/+=]", "").Substring(1, 6);
    //            string strProductionShortPathForBackup = "/files-backups" + strPath + strMakeUnique + strFilename;
    //            string strTestShortPathForBackup = "/files-backups" + strPath + strMakeUnique + strFilename;
    //            string strProductionFullPathForBackup = "ftp://ftp.adpce.ad/files" + "/files-backups" + strPath + strMakeUnique + strFilename;
    //            string strTestFullPathForBackup = "ftp://ftpt.adpce.ad/files" + "/files-backups" + strPath + strMakeUnique + strFilename;

    //            if (strTestSiteOnlyYN.Trim() != "Y") {
    //                if (!FtpDirectoryExists("ftp://ftp.adpce.ad/files" + strPath, strFTPUsername, strFTPPass)) {
    //                    //throw new Exception("Path does not exist " + "ftp://ftp.adpce.ad/files" + strPath + ".");
    //                    FtpMakeFolder("ftp://ftp.adpce.ad/files" + strPath, strFTPUsername, strFTPPass);
    //                }
    //            }
    //            if (!FtpDirectoryExists("ftp://ftpt.adpce.ad/files" + strPath, strFTPUsername, strFTPPass)) {
    //                FtpMakeFolder("ftp://ftpt.adpce.ad/files" + strPath, strFTPUsername, strFTPPass);
    //                //throw new Exception("Path does not exist " + "ftp://ftpt.adpce.ad/files" + strPath + ".");
    //            }

    //            bool booProductionFileExists = FtpFileExists(strProductionPath, strFTPUsername, strFTPPass);
    //            bool booTestFileExists = FtpFileExists(strTestPath, strFTPUsername, strFTPPass);

    //            if (strAction == "Publish") {
    //                PublishFTPFile(strTestPath, strProductionPath, strFTPUsername, strFTPPass);
    //                return null;
    //            } else if (strAction == "Unpublish") {
    //                if (booProductionFileExists) {
    //                    FtpRenameFile(strProductionPath, strProductionShortPathForBackup, strFTPUsername, strFTPPass);
    //                }
    //                return null;
    //            }

    //            if (strTestSiteOnlyYN.Trim() != "Y") {
    //                if (booProductionFileExists) {
    //                    if (strAction == "Delete") {
    //                        FtpRenameFile(strProductionPath, strProductionShortPathForBackup, strFTPUsername, strFTPPass);
    //                    } else {
    //                        throw new Exception("Error: The file " + strFilename + " already exists on the production server.");
    //                    }
    //                    //FtpRenameFile(strProductionPath, strProductionShortPathForBackup, strFTPUsername, strFTPPass);
    //                    // this check doesn't work
    //                    //Security.Log(this.User, "Renamed file from " + strProductionPath + " to " + strProductionShortPathForBackup);
    //                    //if (!FtpFileExists(strProductionFullPathForBackup, strFTPUsername, strFTPPass)) {
    //                    //    throw new Exception("Error creating the file " + strProductionFullPathForBackup);
    //                    //} else {
    //                    //    Security.Log(this.User, "Created and verified the file " + strProductionShortPathForBackup);
    //                    //}
    //                } else {
    //                    Security.Log(this.User, "Didn't find the production file " + strProductionPath);
    //                }
    //            }

    //            if (booTestFileExists) {
    //                if (strAction == "Delete") {
    //                    FtpRenameFile(strTestPath, strTestShortPathForBackup, strFTPUsername, strFTPPass);
    //                } else {
    //                    throw new Exception("Error: The file " + strFilename + " already exists on the test server.");
    //                }
    //                //FtpRenameFile(strTestPath, strTestShortPathForBackup, strFTPUsername, strFTPPass);
    //                // this check doesn't work
    //                //Security.Log(this.User, "Renamed file from " + strTestPath + " to " + strTestShortPathForBackup);
    //                //if (!FtpFileExists(strTestFullPathForBackup, strFTPUsername, strFTPPass)) {
    //                //    throw new Exception("Error creating the file " + strTestFullPathForBackup);
    //                //} else {
    //                //    Security.Log(this.User, "Created and verified the file " + strTestShortPathForBackup);
    //                //}
    //            } else {
    //                Security.Log(this.User, "Didn't find the test file " + strTestPath);
    //            }

    //            if (strAction == "Delete") {
    //                return null;
    //            }

    //            if (true) {// reduce scope
    //                FtpWebRequest oFtpWebRequest =
    //                    (FtpWebRequest)WebRequest.Create("ftp://ftp.adpce.ad/files" + strPath + strFilename);
    //                oFtpWebRequest.Credentials = new NetworkCredential(strFTPUsername, strFTPPass);
    //                oFtpWebRequest.Method = WebRequestMethods.Ftp.UploadFile;

    //                using (System.IO.Stream oStream = oHttpPostedFile.InputStream)
    //                using (System.IO.Stream ftpStream = oFtpWebRequest.GetRequestStream()) {
    //                    oStream.CopyTo(ftpStream);
    //                }
    //            }

    //            if (strTestSiteOnlyYN.Trim() != "Y") {
    //                if (true) {// reduce scope
    //                    FtpWebRequest oFtpWebRequest =
    //                        (FtpWebRequest)WebRequest.Create("ftp://ftpt.adpce.ad/files" + strPath + strFilename);
    //                    oFtpWebRequest.Credentials = new NetworkCredential(strFTPUsername, strFTPPass);
    //                    oFtpWebRequest.Method = WebRequestMethods.Ftp.UploadFile;

    //                    using (System.IO.Stream oStream = oHttpPostedFile.InputStream)
    //                    using (System.IO.Stream ftpStream = oFtpWebRequest.GetRequestStream()) {
    //                        oStream.CopyTo(ftpStream);
    //                    }
    //                }
    //            }

    //            //Security.Log(this.User, "File saved " + strFullPath);
    //            return null;
    //        } catch (Exception e) {
    //            DataTable dtErrorData = new DataTable();
    //            dtErrorData.Clear();
    //            dtErrorData.Columns.Add("WebAPIMessageType");
    //            dtErrorData.Columns.Add("WebAPIMessage");
    //            DataRow oDataRow = dtErrorData.NewRow();
    //            oDataRow["WebAPIMessageType"] = "Error";
    //            oDataRow["WebAPIMessage"] = e.Message;
    //            dtErrorData.Rows.Add(oDataRow);
    //            Security.Log(this.User, "Error " + e.Message + " - " + strDebugValues);
    //            return dtErrorData;
    //        }
    //    }

    //    private static bool FtpDirectoryExists(string directory, string username, string password) {
    //        try {
    //            var oFtpWebRequest = (FtpWebRequest)WebRequest.Create(directory);
    //            oFtpWebRequest.Credentials = new NetworkCredential(username, password);
    //            oFtpWebRequest.Method = WebRequestMethods.Ftp.ListDirectory;

    //            FtpWebResponse oFtpWebResponse = (FtpWebResponse)oFtpWebRequest.GetResponse();
    //            oFtpWebResponse.Close();
    //        } catch (WebException ex) {
    //            FtpWebResponse oFtpWebResponse = (FtpWebResponse)ex.Response;
    //            if (oFtpWebResponse.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable) {
    //                oFtpWebResponse.Close();
    //                return false;
    //            } else {
    //                oFtpWebResponse.Close();
    //                return true;
    //            }
    //        }
    //        return true;

    //    }
    //    private static bool FtpFileExists(string filepath, string username, string password) {
    //        FtpWebRequest oWebRequestDoesFileExist = (FtpWebRequest)WebRequest.Create(filepath);
    //        oWebRequestDoesFileExist.Credentials = new NetworkCredential(username, password);
    //        oWebRequestDoesFileExist.Method = WebRequestMethods.Ftp.GetFileSize;
    //        //oWebRequestDoesFileExist.Method = WebRequestMethods.Ftp.ListDirectory;

    //        try {
    //            FtpWebResponse oFtpWebResponse = (FtpWebResponse)oWebRequestDoesFileExist.GetResponse();
    //            if (oFtpWebResponse.StatusCode == FtpStatusCode.FileActionOK || oFtpWebResponse.StatusCode == FtpStatusCode.FileStatus) {
    //                oFtpWebResponse.Close();
    //                return true;
    //            } else {
    //                //oFtpWebResponse.Close();
    //                throw new Exception("Problem checking for existence of " + filepath + ".  Status " + oFtpWebResponse.StatusDescription);
    //            }
    //        } catch (WebException ex) {
    //            FtpWebResponse oFtpWebResponse = (FtpWebResponse)ex.Response;
    //            if (oFtpWebResponse.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable) {
    //                //Does not exist
    //                oFtpWebResponse.Close();
    //                return false;
    //            } else {
    //                oFtpWebResponse.Close();
    //                throw new Exception("Error checking for existence of " + filepath + " Error: " + ex.Message);
    //            }
    //        }
    //    }

    //    private static void FtpRenameFile(string filepath, string newfilepath, string username, string password) {
    //        try {
    //            FtpWebRequest oWebRequestRename = (FtpWebRequest)WebRequest.Create(filepath);
    //            oWebRequestRename.Credentials = new NetworkCredential(username, password);
    //            oWebRequestRename.Method = WebRequestMethods.Ftp.Rename;
    //            oWebRequestRename.RenameTo = newfilepath;

    //            FtpWebResponse oFtpWebResponse = (FtpWebResponse)oWebRequestRename.GetResponse();
    //            oFtpWebResponse.Close();
    //        } catch (WebException ex) {
    //            FtpWebResponse oFtpWebResponse = (FtpWebResponse)ex.Response;
    //            oFtpWebResponse.Close();
    //            throw new Exception("Error deleting file " + filepath + " " + ex.Message);
    //        }
    //    }

    //    private static void FtpMakeFolder(string folderpath, string username, string password) {
    //        try {
    //            FtpWebRequest oWebRequestRename = (FtpWebRequest)WebRequest.Create(folderpath);
    //            oWebRequestRename.Credentials = new NetworkCredential(username, password);
    //            oWebRequestRename.Method = WebRequestMethods.Ftp.MakeDirectory;

    //            FtpWebResponse oFtpWebResponse = (FtpWebResponse)oWebRequestRename.GetResponse();
    //            oFtpWebResponse.Close();
    //        } catch (WebException ex) {
    //            FtpWebResponse oFtpWebResponse = (FtpWebResponse)ex.Response;
    //            oFtpWebResponse.Close();
    //            throw new Exception("Error creating folder " + folderpath + " " + ex.Message);
    //        }
    //    }


    //    public static bool PublishFTPFile(string fileName, string DestinationFile, string userName, string password) {
    //        try {
    //            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(fileName);
    //            request.Method = WebRequestMethods.Ftp.DownloadFile;

    //            request.Credentials = new NetworkCredential(userName, password);
    //            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
    //            System.IO.Stream responseStream = response.GetResponseStream();
    //            Upload(DestinationFile, ToByteArray(responseStream), userName, password);
    //            responseStream.Close();
    //            return true;
    //        } catch {
    //            return false;
    //        }
    //    }

    //    public static Byte[] ToByteArray(System.IO.Stream stream) {
    //        System.IO.MemoryStream ms = new System.IO.MemoryStream();
    //        byte[] chunk = new byte[4096];
    //        int bytesRead;
    //        while ((bytesRead = stream.Read(chunk, 0, chunk.Length)) > 0) {
    //            ms.Write(chunk, 0, bytesRead);
    //        }

    //        return ms.ToArray();
    //    }

    //    public static bool Upload(string FileName, byte[] Image, string FtpUsername, string FtpPassword) {
    //        try {
    //            System.Net.FtpWebRequest clsRequest = (System.Net.FtpWebRequest)System.Net.WebRequest.Create(FileName);
    //            clsRequest.Credentials = new System.Net.NetworkCredential(FtpUsername, FtpPassword);
    //            clsRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
    //            System.IO.Stream clsStream = clsRequest.GetRequestStream();
    //            clsStream.Write(Image, 0, Image.Length);

    //            clsStream.Close();
    //            clsStream.Dispose();
    //            return true;
    //        } catch {
    //            return false;
    //        }
    //    }



    }
}

