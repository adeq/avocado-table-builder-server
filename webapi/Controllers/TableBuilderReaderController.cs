﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Data;
using System.Data.Odbc;
namespace WEBAPI_JWT_Authentication.Controllers {
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    //[Authorize]
    public class TableBuilderReaderController : ApiController {
        [HttpPost]
        public DataTable GetListOfSections([FromBody]TableBuilderReaderSectionTitles oSearchPageRequest) {
            DataTable dtData = new DataTable();
            try {
                if (!Security.IsInternalADEQIP(this.User)) {
                    // Only for internal ADEQ users
                    throw new Exception("Access Denied");
                }

                oSearchPageRequest.Path = Security.ProtectInjection(oSearchPageRequest.Path);
                oSearchPageRequest.SectionTitleStartsWith = Security.ProtectInjectionWithLikeClause(oSearchPageRequest.SectionTitleStartsWith);

                OdbcCommand cmdOdbcCommand = new OdbcCommand();
                OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
                OdbcConnection connConnection;
                connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Reader"].ToString());
                cmdOdbcCommand.CommandType = CommandType.Text;
                cmdOdbcCommand.CommandText = "EXEC up_GetTableBuilderListSections @Path ='" + oSearchPageRequest.Path + "', @SectionTitleStartsWith = '" + oSearchPageRequest.SectionTitleStartsWith + "'";

                cmdOdbcCommand.Connection = connConnection;
                daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
                daSqlDataAdapter.Fill(dtData);

                return dtData;
            } catch (Exception e) {
                DataTable dtErrorData = new DataTable();
                dtErrorData.Clear();
                dtErrorData.Columns.Add("WebAPIMessageType");
                dtErrorData.Columns.Add("WebAPIMessage");
                DataRow oDataRow = dtErrorData.NewRow();
                oDataRow["WebAPIMessageType"] = "Error";
                oDataRow["WebAPIMessage"] = e.Message;
                dtErrorData.Rows.Add(oDataRow);

                Security.Log(this.User, "Error " + e.Message);
                return dtErrorData;
            }
        }

        [HttpPost]
        public DataTable GetListOfPaths([FromBody]TableBuilderReaderGetPaths oPageRequest) {
            DataTable dtData = new DataTable();
            try {
                if (!Security.IsInternalADEQIP(this.User)) {
                    // Only for internal ADEQ users
                    throw new Exception("Access Denied");
                }

                oPageRequest.PathStartsWith = Security.ProtectInjectionWithLikeClause(oPageRequest.PathStartsWith);

                OdbcCommand cmdOdbcCommand = new OdbcCommand();
                OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
                OdbcConnection connConnection;
                connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Reader"].ToString());
                cmdOdbcCommand.CommandType = CommandType.Text;
                cmdOdbcCommand.CommandText = "EXEC up_GetTableBuilderListPaths @PathStartsWith ='" + oPageRequest.PathStartsWith + "'";

                cmdOdbcCommand.Connection = connConnection;
                daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
                daSqlDataAdapter.Fill(dtData);

                return dtData;
            } catch (Exception e) {
                DataTable dtErrorData = new DataTable();
                dtErrorData.Clear();
                dtErrorData.Columns.Add("WebAPIMessageType");
                dtErrorData.Columns.Add("WebAPIMessage");
                DataRow oDataRow = dtErrorData.NewRow();
                oDataRow["WebAPIMessageType"] = "Error";
                oDataRow["WebAPIMessage"] = e.Message;
                dtErrorData.Rows.Add(oDataRow);

                Security.Log(this.User, "Error " + e.Message);
                return dtErrorData;
            }
        }

        [HttpPost]
        public DataTable GetTableBuilder([FromBody] TableBuilderReaderSectionTitle oTableBuilder) {
            DataTable dtData = new DataTable();
            try {
                if (!Security.IsInternalADEQIP(this.User)) {
                    // Only for internal ADEQ users
                    throw new Exception("Access Denied");
                }

                oTableBuilder.Path = Security.ProtectInjection(oTableBuilder.Path);
                oTableBuilder.SectionTitle = Security.ProtectInjectionWithLikeClause(oTableBuilder.SectionTitle);

                OdbcCommand cmdOdbcCommand = new OdbcCommand();
                OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
                OdbcConnection connConnection;
                connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Reader"].ToString());
                cmdOdbcCommand.CommandType = CommandType.Text;
                cmdOdbcCommand.CommandText = "EXEC up_GetTableBuilderRecordFromSectionTitle @Path ='" +
                    oTableBuilder.Path + "', @SectionTitle = '" + oTableBuilder.SectionTitle + "'";

                cmdOdbcCommand.Connection = connConnection;
                daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
                daSqlDataAdapter.Fill(dtData);

                return dtData;
            } catch (Exception e) {
                DataTable dtErrorData = new DataTable();
                dtErrorData.Clear();
                dtErrorData.Columns.Add("WebAPIMessageType");
                dtErrorData.Columns.Add("WebAPIMessage");
                DataRow oDataRow = dtErrorData.NewRow();
                oDataRow["WebAPIMessageType"] = "Error";
                oDataRow["WebAPIMessage"] = e.Message;
                dtErrorData.Rows.Add(oDataRow);

                Security.Log(this.User, "Error " + e.Message);
                return dtErrorData;
            }
        }


        [HttpPost]
        public DataTable GetWebsiteFileListsByPath([FromBody] TableBuilderReaderPath oRequest) {
            DataTable dtData = new DataTable();
            DataTable dtUsers = new DataTable();
            try {
                if (!Security.IsInternalADEQIP(this.User)) {
                    // Only for internal ADEQ users
                    throw new Exception("Access Denied");
                }

                oRequest.Path = Security.ProtectInjection(oRequest.Path);

                OdbcCommand cmdOdbcCommand = new OdbcCommand();
                OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
                OdbcConnection connConnection;
                connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-TableBuilder_Reader"].ToString());
                cmdOdbcCommand.CommandType = CommandType.Text;
                cmdOdbcCommand.CommandText = "EXEC up_GetWebsiteFileListsRecordByPath @Path ='" +
                    oRequest.Path + "'";

                cmdOdbcCommand.Connection = connConnection;
                daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
                daSqlDataAdapter.Fill(dtData);

                return dtData;
            } catch (Exception e) {
                DataTable dtErrorData = new DataTable();
                dtErrorData.Clear();
                dtErrorData.Columns.Add("WebAPIMessageType");
                dtErrorData.Columns.Add("WebAPIMessage");
                DataRow oDataRow = dtErrorData.NewRow();
                oDataRow["WebAPIMessageType"] = "Error";
                oDataRow["WebAPIMessage"] = e.Message;
                dtErrorData.Rows.Add(oDataRow);

                Security.Log(this.User, "Error " + e.Message);
                return dtErrorData;
            }
        }


        [HttpGet]
        public DataTable GetFullEmployeeInformationList() {
            DataTable dtData = new DataTable();
            try {
                if (!Security.IsInternalADEQIP(this.User)) {
                    // Only for internal ADEQ users
                    throw new Exception("Access Denied");
                }
                OdbcCommand cmdOdbcCommand = new OdbcCommand();
                OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
                OdbcConnection connConnection;
                connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-ProgramSettings_Reader"].ToString());
                cmdOdbcCommand.CommandType = CommandType.Text;
                cmdOdbcCommand.CommandText = "EXEC dbo.up_GetAllUsersInfo ";
                cmdOdbcCommand.Connection = connConnection;
                daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
                daSqlDataAdapter.Fill(dtData);

                return dtData;
            } catch (Exception e) {
                DataTable dtErrorData = new DataTable();
                dtErrorData.Clear();
                dtErrorData.Columns.Add("WebAPIMessageType");
                dtErrorData.Columns.Add("WebAPIMessage");
                DataRow oDataRow = dtErrorData.NewRow();
                oDataRow["WebAPIMessageType"] = "Error";
                oDataRow["WebAPIMessage"] = e.Message;
                dtErrorData.Rows.Add(oDataRow);
                return dtErrorData;
            }
        }
    }
}


public class TableBuilderReaderSectionTitles {
    public string Path { get; set; }
    public string SectionTitleStartsWith { get; set; }
}

public class TableBuilderReaderGetPaths {
    public string PathStartsWith { get; set; }
}

public class TableBuilderReaderSectionTitle {
    public string Path { get; set; }
    public string SectionTitle { get; set; }
}
public class TableBuilderReaderPath {
    public string Path { get; set; }
}