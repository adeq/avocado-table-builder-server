﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;
using System.Net.Http;

namespace WEBAPI_JWT_Authentication.Controllers {
    public class Security {
        public static string ProtectInjection(string Value) {
            if (Value == null) return "";
            return Value.Replace("'", "''");
        }
        public static string ProtectInjectionWithLikeClause(string Value) {
            return Value.Replace("'", "''").Replace("[", "[[]");
        }
        public static Boolean CheckAuthorization(string Username, string Permission) {
            DataTable dtData = new DataTable();
            try {
                OdbcCommand cmdOdbcCommand = new OdbcCommand();
                OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
                OdbcConnection connConnection;
                connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-ProgramSettings_Reader"].ToString());
                cmdOdbcCommand.CommandType = CommandType.Text;
                cmdOdbcCommand.CommandText = " EXECUTE up_SMgr_GetPermissions @UserID  = '" + Username + "' ";
                cmdOdbcCommand.Connection = connConnection;
                daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
                daSqlDataAdapter.Fill(dtData);

                return (dtData.Select("PermissionGroup='" + Permission + "' ").Count() == 1);
            } catch (Exception e) {
                DataTable dtErrorData = new DataTable();
                dtErrorData.Clear();
                dtErrorData.Columns.Add("WebAPIMessageType");
                dtErrorData.Columns.Add("WebAPIMessage");
                DataRow oDataRow = dtErrorData.NewRow();
                oDataRow["WebAPIMessageType"] = "Error";
                oDataRow["WebAPIMessage"] = e.Message;
                dtErrorData.Rows.Add(oDataRow);
                return false;
            }
        }

        public static string GetIP() {
            string strIpAddress = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (!string.IsNullOrEmpty(strIpAddress)) {
                string[] addresses = strIpAddress.Split(',');
                if (addresses.Length != 0) {
                    strIpAddress = addresses[0];
                    if (strIpAddress.Contains(":")) {
                        strIpAddress = strIpAddress.Split(':')[0];
                    }
                }
            }
            return strIpAddress;
        }

        public static bool IsInternalADEQIP(System.Security.Principal.IPrincipal User) {
            if (Security.CheckAuthorization(User.Identity.Name.ToLower(), "WebsiteCommon_TreatAsExpernalIPAlwaysPriv")) {
                return false;
            }

            string strIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(
                                    "(^127\\.)|(^10\\.)|(^172\\.1[6-9]\\.)|(^172\\.2[0-9]\\.)|(^172\\.3[0-1]\\.)|(^192\\.168\\.)",
                                    System.Text.RegularExpressions.RegexOptions.IgnoreCase |
                                    System.Text.RegularExpressions.RegexOptions.Singleline);
            System.Text.RegularExpressions.Match m = r.Match(strIP);

            if (!string.IsNullOrEmpty(strIP)) {
                if (m.Success) {
                    return true;
                }
            }

            //if (!string.IsNullOrEmpty(strIP)) {
            //    string[] addresses = strIP.Split(',');
            //    if (addresses.Length != 0) {
            //        strIP = addresses[0];
            //        if (strIP.StartsWith("172.27.") || strIP.StartsWith("172.29.")) {
            //            return true;
            //        }
            //    }
            //}
            return false;
        }

        public static DataTable AuthyUpdateSecurityCheck(System.Security.Principal.IPrincipal User) {
            bool booGetCode = false;
            if (!IsInternalADEQIP(User)) {
                if (IsUserBlockedByAuthy(User)) {
                    booGetCode = true;
                }

                Random oRandomAuthyCheck = new Random();
                string strMaxChance = System.Configuration.ConfigurationManager.AppSettings["MaxRandomChanceForAuthyReverity"].ToString();
                int intMaxChance = 5;//default
                if (int.TryParse(strMaxChance, out int value)) {
                    intMaxChance = int.Parse(strMaxChance);
                }
                if (oRandomAuthyCheck.Next(1, intMaxChance + 1) == 1) {
                    // 1 in 5 chance to have to reverify
                    ForceAuthyReverify(User);
                    booGetCode = true;
                }
            }
            if (booGetCode) {
                DataTable dtErrorData = new DataTable();
                dtErrorData.Columns.Add("WebAPIMessageType");
                dtErrorData.Columns.Add("WebAPIMessage");
                DataRow oDataRow = dtErrorData.NewRow();
                oDataRow["WebAPIMessageType"] = "GetAuthyCode";
                oDataRow["WebAPIMessage"] = "Enter Authy code.";
                dtErrorData.Rows.Add(oDataRow);

                return dtErrorData;
            } else {
                return null;
            }
        }

        private static bool IsUserBlockedByAuthy(System.Security.Principal.IPrincipal User) {
            DataTable dtData = new DataTable();
            OdbcCommand cmdOdbcCommand = new OdbcCommand();
            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
            OdbcConnection connConnection;
            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-AuthyUpdater"].ToString());
            cmdOdbcCommand.CommandType = CommandType.Text;
            cmdOdbcCommand.CommandText = " EXECUTE up_GetAuthyRecord @Username = '" + User.Identity.Name.ToLower() + "' ";
            cmdOdbcCommand.Connection = connConnection;
            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
            daSqlDataAdapter.Fill(dtData);

            if (dtData.Rows.Count != 1) {
                return true;
            }
            DataRow drAuthy = dtData.Rows[0];
            string strRecheckID = dtData.Rows[0]["RecheckID"].ToString();
            string strLastIP = dtData.Rows[0]["IP"].ToString();

            if (strRecheckID != "") {
                using (var httpClient = new HttpClient()) {
                    httpClient.DefaultRequestHeaders.Add("X-Authy-API-Key", System.Configuration.ConfigurationManager.AppSettings["AuthyKey"]);
                    HttpResponseMessage response = httpClient.GetAsync("https://api.authy.com/onetouch/json/approval_requests/" + drAuthy["RecheckID"].ToString()).Result;
                    string result = response.Content.ReadAsStringAsync().Result;
                    if (result.Contains("\"status\":\"approved\"")) {
                        Security.Log(User, "User approved onetouch.");
                        DataTable dtData2 = new DataTable();
                        cmdOdbcCommand = new OdbcCommand();
                        daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
                        connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-AuthyUpdater"].ToString());
                        cmdOdbcCommand.CommandType = CommandType.Text;
                        cmdOdbcCommand.CommandText = " EXECUTE up_UpdateAuthyRecord " +
                              "@RecordAction = 'U' " +
                            ", @Username = '" + User.Identity.Name.ToLower() + "' " +
                            ", @AuthyID = '" + drAuthy["AuthyID"].ToString() + "' " +
                            ", @Email = '" + drAuthy["Email"].ToString() + "' " +
                            ", @Phone = '" + drAuthy["Phone"].ToString() + "' " +
                            ", @LastLogin = '" + drAuthy["LastLogin"].ToString() + "' " +
                            ", @LastFailedLogin = '" + drAuthy["LastFailedLogin"].ToString() + "' " +
                            ", @IP = '" + Security.GetIP() + "' " +
                            ", @WaitingForRecheckYN = 'N' " +
                            ", @RecheckID = '' ";
                        cmdOdbcCommand.Connection = connConnection;
                        daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
                        daSqlDataAdapter.Fill(dtData2);
                        return false;
                    }
                }
            } else if (strLastIP != GetIP()) {
                ForceAuthyReverify(User);
                return true;
            }

            return dtData.Rows[0]["WaitingForRecheckYN"].ToString() == "Y";
        }

        public static DataRow getAuthyData(string Username) {
            DataTable dtData = new DataTable();
            OdbcCommand cmdOdbcCommand = new OdbcCommand();
            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
            OdbcConnection connConnection;
            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-AuthyUpdater"].ToString());
            cmdOdbcCommand.CommandType = CommandType.Text;
            cmdOdbcCommand.CommandText = " EXECUTE up_GetAuthyRecord @Username = '" + Username + "' ";
            cmdOdbcCommand.Connection = connConnection;
            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
            daSqlDataAdapter.Fill(dtData);

            if (dtData.Rows.Count == 1) {
                return dtData.Rows[0];
            } else {
                return null;
            }
        }

        public static void ForceAuthyReverify(System.Security.Principal.IPrincipal User) {

            try {
                DataTable dtAuthyData = new DataTable();
                DataRow drAuthy = null;
                OdbcCommand cmdOdbcCommand = new OdbcCommand();
                OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
                OdbcConnection connConnection;
                connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-AuthyUpdater"].ToString());
                cmdOdbcCommand.CommandType = CommandType.Text;
                cmdOdbcCommand.CommandText = " EXECUTE up_GetAuthyRecord @Username = '" + User.Identity.Name.ToLower() + "' ";
                cmdOdbcCommand.Connection = connConnection;
                daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
                daSqlDataAdapter.Fill(dtAuthyData);

                if (dtAuthyData.Rows.Count == 1) {
                    drAuthy = dtAuthyData.Rows[0];
                } else {
                    drAuthy = null;
                }

                // Send a one touch request
                string strApprovalRequest = "";
                if (drAuthy != null) {
                    AuthyOT.OneTouchClient oOT = new AuthyOT.OneTouchClient(System.Configuration.ConfigurationManager.AppSettings["AuthyKey"], drAuthy["AuthyID"].ToString());
                    AuthyOT.SendApprovalRequestResult oOTResult = oOT.SendApprovalRequest("Please approve your ADEQ Intranet update request.", drAuthy["Email"].ToString());
                    if (!oOTResult.Success) {
                        throw new Exception("Error while sending approval request.");
                    }
                    foreach (KeyValuePair<string, string> entry in oOTResult.ApprovalRequest) {
                        strApprovalRequest += "Key: " + entry.Key.ToString() + ", Val: " + entry.Value.ToString();
                        if (entry.Key.ToString() == "uuid") {
                            strApprovalRequest = entry.Value.ToString();
                            break;
                        }
                    }
                    Security.Log(User, "Send Authy approval request.  Message: " + oOTResult.Message + ", Status: " + oOTResult.Status.ToString() + ", Success: " + oOTResult.Success.ToString() + ", ApprovalRequest: " + strApprovalRequest);
                    if (strApprovalRequest.Length == 0) {
                        throw new Exception("Error while sending approval request. No uuid.");
                    }
                }

                DataTable dtData = new DataTable();
                cmdOdbcCommand = new OdbcCommand();
                daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
                connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-AuthyUpdater"].ToString());
                cmdOdbcCommand.CommandType = CommandType.Text;
                cmdOdbcCommand.CommandText = " EXECUTE up_UpdateAuthyRecord " +
                       "@RecordAction = 'U' " +
                    ", @Username = '" + User.Identity.Name.ToLower() + "' " +
                    ", @AuthyID = '" + drAuthy["AuthyID"].ToString() + "' " +
                    ", @Email = '" + drAuthy["Email"].ToString() + "' " +
                    ", @Phone = '" + drAuthy["Phone"].ToString() + "' " +
                    ", @LastLogin = '" + drAuthy["LastLogin"].ToString() + "' " +
                    ", @LastFailedLogin = '" + drAuthy["LastFailedLogin"].ToString() + "' " +
                    ", @IP = '" + Security.GetIP() + "' " +
                    ", @WaitingForRecheckYN = 'Y' " +
                    ", @RecheckID = '" + strApprovalRequest + "'";
                cmdOdbcCommand.Connection = connConnection;
                daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
                daSqlDataAdapter.Fill(dtData);
            } catch (Exception e) {
                Log(User, "Error setting Authy data for recheck.  " + e.Message);
                throw;
            }
        }

        public static void Log(System.Security.Principal.IPrincipal User, string Message) {
            DataTable dtData = new DataTable();
            OdbcCommand cmdOdbcCommand = new OdbcCommand();
            OdbcDataAdapter daSqlDataAdapter = new System.Data.Odbc.OdbcDataAdapter();
            OdbcConnection connConnection;

            Message = "url: " + System.Web.HttpContext.Current.Request.Url + " " + Message;
            Message = "user: " + User.Identity.Name.ToLower() + " IP: " + GetIP() + " " + Message;

            if (Message.Length > 1000) Message = Message.Substring(0, 1000);
            connConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings["connection-string-WebsiteData_Updater"].ToString());
            cmdOdbcCommand.CommandType = CommandType.Text;
            cmdOdbcCommand.CommandText = " EXECUTE up_InsertLogMessage @Message = '" + Message.Replace("'", "''") + "' ";
            cmdOdbcCommand.Connection = connConnection;
            daSqlDataAdapter.SelectCommand = cmdOdbcCommand;
            daSqlDataAdapter.Fill(dtData);
        }
    }
}
