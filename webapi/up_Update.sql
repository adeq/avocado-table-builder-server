USE [TableBuilder]
GO
/****** Object:  StoredProcedure [dbo].[up_Update]    Script Date: 9/20/2018 6:31:27 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[up_Update]
	(@What varchar(50) = '',
	-- Table, Table Group, Field
	@Action varchar(10) = '',
	-- Delete, Update, Insert
	@ID varchar(50) = '',
	@Value varchar(max) = '',
	@CreatedBy varchar(20) = '',
	@CreatedByID varchar(20) = NULL)
AS
BEGIN
	SET NOCOUNT ON;
	IF (@What IS NULL
		OR RTRIM(LTRIM(@What)) = '') BEGIN
		SELECT 'Error'[WebAPIMessageType], '@What is required.'[WebAPIMessage]
		RAISERROR ('@What is required.', 20, -1) WITH LOG
	END
	IF (@What IS NULL
		OR RTRIM(LTRIM(@Action)) = '') BEGIN
		SELECT 'Error'[WebAPIMessageType], '@Action is required.'[WebAPIMessage]
		RAISERROR ('@Action is required.', 20, -1) WITH LOG
	END
	IF (@ID IS NULL
		OR RTRIM(LTRIM(@Action)) = '') BEGIN
		SELECT 'Error'[WebAPIMessageType], '@ID is required.'[WebAPIMessage]
		RAISERROR ('@ID is required.', 20, -1) WITH LOG
	END

	DECLARE @StringToSplit varchar(max) = '';
	IF (LTRIM(RTRIM(@Value)) <> '') BEGIN
		SET @StringToSplit = @Value + ',';
	END

	DECLARE @SplitTable TABLE (
		[val] [varchar](MAX))
	DECLARE @EachSplitValue varchar(max)
	DECLARE @CommaPosition int
	WHILE CHARINDEX(',', @StringToSplit) > 0 BEGIN
		SELECT
			@CommaPosition = CHARINDEX(',', @StringToSplit)
		SELECT
			@EachSplitValue = SUBSTRING(@stringToSplit, 1, @CommaPosition - 1)
		INSERT INTO @SplitTable
		SELECT
			REPLACE(RTRIM(LTRIM(@EachSplitValue)), '[[{{comma}}]]', ',')
		SELECT
			@stringToSplit = SUBSTRING(@StringToSplit, @CommaPosition + 1, LEN(@StringToSplit) - @CommaPosition)
	END

	--------------

	IF @What = 'table' AND @Action = 'update' BEGIN
		BEGIN TRY
		BEGIN TRAN

		DECLARE @NewTableTitle varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable

		DECLARE @NewTableComments varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable

		DECLARE @NewUpdateTableTriggerSQL varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = @ID
			AND Type = 'table-right-editor'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table editor rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'table')
			AND tab_right_group.Type = 'table-group-right-editor'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table group editor rights missing.', 20, -1) WITH LOG
		END

		IF EXISTS (SELECT TOP 1
			Value
		FROM config
		WHERE type = 'table' AND value = @NewTableTitle AND id <> @ID) BEGIN
			DECLARE @UniqueUpdateTableNameError varchar(max) = 'A table with the name ' + @NewTableTitle + ' already exists.';
			SELECT 'Error'[WebAPIMessageType], @UniqueUpdateTableNameError[WebAPIMessage]
			RAISERROR (@UniqueUpdateTableNameError, 20, -1) WITH LOG
		END

		-- make a copy of the original record with a new ID
		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ID = @ID AND Type = 'table'

		UPDATE Config
		SET value = @NewTableTitle,
			ModifiedBy = @CreatedBy,
			ModifiedByID = @CreatedByID,
			ModifiedDate = GETDATE()
		WHERE ID = @ID AND Type = 'table'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'table-comment'

		UPDATE Config
		SET value = @NewTableComments,
			ModifiedBy = @CreatedBy,
			ModifiedByID = @CreatedByID,
			ModifiedDate = GETDATE()
		WHERE ParentID = @ID AND Type = 'table-comment'

		INSERT INTO BigDataValuesArchive
			(OriginalID, Value,ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT big.id, big.Value, @CreatedBy, @CreatedByID, GETDATE()
		FROM config table_
			left outer join BigDataValues big on table_.DataLinkID = big.id
		WHERE table_.id = @ID AND table_.type = 'table-sql-trigger'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'table-sql-trigger'

		UPDATE BigDataValues 
		SET Value = @NewUpdateTableTriggerSQL
		WHERE ID = (SELECT DataLinkID
		FROM Config
		WHERE ParentID = @ID
			AND Type = 'table-sql-trigger')

		UPDATE Config
		SET ModifiedBy = @CreatedBy,
			ModifiedByID = @CreatedByID,
			ModifiedDate = GETDATE()
		WHERE ParentID = @ID
			AND Type = 'table-sql-trigger'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND (Type = 'table-right-designer' OR Type = 'table-right-editor' OR Type = 'table-right-viewer')
		DELETE FROM Config
		WHERE ParentID = @ID AND (Type = 'table-right-designer' OR Type = 'table-right-editor' OR Type = 'table-right-viewer')

		DECLARE @RightsUpdateValue VARCHAR(max);
		DECLARE RightsUpdate_cursor CURSOR FOR
		SELECT val
		FROM @SplitTable

		  OPEN RightsUpdate_cursor

		  FETCH NEXT FROM RightsUpdate_cursor
		  INTO @RightsUpdateValue

		  WHILE @@FETCH_STATUS = 0
		  BEGIN
			IF LEFT(@RightsUpdateValue, 1) = 'd' BEGIN
				SET @RightsUpdateValue = LTRIM(RTRIM(SUBSTRING(@RightsUpdateValue, 2, LEN(@RightsUpdateValue))));
				IF @RightsUpdateValue <> ''
				BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@ID, 'table-right-designer', @RightsUpdateValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			ELSE IF LEFT(@RightsUpdateValue, 1) = 'e' BEGIN
				SET @RightsUpdateValue = LTRIM(RTRIM(SUBSTRING(@RightsUpdateValue, 2, LEN(@RightsUpdateValue))));
				IF @RightsUpdateValue <> ''
				BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@ID, 'table-right-editor', @RightsUpdateValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			ELSE IF LEFT(@RightsUpdateValue, 1) = 'v' BEGIN
				SET @RightsUpdateValue = LTRIM(RTRIM(SUBSTRING(@RightsUpdateValue, 2, LEN(@RightsUpdateValue))));
				IF @RightsUpdateValue <> ''
				BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@ID, 'table-right-viewer', @RightsUpdateValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			FETCH NEXT FROM RightsUpdate_cursor
			INTO @RightsUpdateValue
		END

		  CLOSE RightsUpdate_cursor;
		  DEALLOCATE RightsUpdate_cursor;

		COMMIT TRAN
	  END TRY
	  BEGIN CATCH
		IF (@@TRANCOUNT > 0)
		  ROLLBACK TRAN;

		THROW; -- raise error to the client
	  END CATCH
	END

	--------------

	IF @What = 'table' AND @Action = 'insert' BEGIN
		BEGIN TRY
		BEGIN TRAN

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = @ID
			AND tab_right_group.Type = 'table-group-right-editor'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table group editor rights missing.', 20, -1) WITH LOG
		END

		DECLARE @NewInsertTableTitle varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		IF EXISTS (SELECT TOP 1
			Value
		FROM config
		WHERE type = 'table' AND id = @ID AND value = @NewInsertTableTitle) BEGIN
			DECLARE @UniqueInsertTableNameError varchar(max) = 'A table with the name ' + @NewInsertTableTitle + ' already exists.';
			SELECT 'Error'[WebAPIMessageType], @UniqueInsertTableNameError[WebAPIMessage]
			RAISERROR (@UniqueInsertTableNameError, 20, -1) WITH LOG
		END

		DELETE TOP (1) FROM @SplitTable

		DECLARE @NewInsertTableComment varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		DELETE TOP (1) FROM @SplitTable

		DECLARE @NewInsertTableTriggerSQL varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		INSERT INTO Config
			(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@ID, 'table', @NewInsertTableTitle, @CreatedBy, @CreatedByID, GETDATE())

		DECLARE @LastInsertedTableID int = SCOPE_IDENTITY();

		INSERT INTO Config
			(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@LastInsertedTableID, 'table-comment', @NewInsertTableComment, @CreatedBy, @CreatedByID, GETDATE())

		INSERT INTO BigDataValues
			(Value)
		VALUES
			(@NewInsertTableTriggerSQL)

		DECLARE @LastInsertedBigDataValueID int = SCOPE_IDENTITY();

		INSERT INTO Config
			(ParentID, Type, DataLinkID, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@LastInsertedTableID, 'table-sql-trigger', @LastInsertedBigDataValueID, @CreatedBy, @CreatedByID, GETDATE())

		DECLARE @RightsInsertValue VARCHAR(max);
		DECLARE RightsInsert_cursor CURSOR FOR
		SELECT val
		FROM @SplitTable

		  OPEN RightsInsert_cursor

		  FETCH NEXT FROM RightsInsert_cursor
		  INTO @RightsInsertValue

		  WHILE @@FETCH_STATUS = 0 BEGIN
			IF LEFT(@RightsInsertValue, 1) = 'd' BEGIN
				SET @RightsInsertValue = LTRIM(RTRIM(SUBSTRING(@RightsInsertValue, 2, LEN(@RightsInsertValue))));
				IF @RightsInsertValue <> ''
				BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@LastInsertedTableID, 'table-right-designer', @RightsInsertValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			ELSE IF LEFT(@RightsInsertValue, 1) = 'e' BEGIN
				SET @RightsInsertValue = LTRIM(RTRIM(SUBSTRING(@RightsInsertValue, 2, LEN(@RightsInsertValue))));
				IF @RightsInsertValue <> ''
				BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@LastInsertedTableID, 'table-right-editor', @RightsInsertValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			ELSE IF LEFT(@RightsInsertValue, 1) = 'v' BEGIN
				SET @RightsInsertValue = LTRIM(RTRIM(SUBSTRING(@RightsInsertValue, 2, LEN(@RightsInsertValue))));
				IF @RightsInsertValue <> ''
				BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@LastInsertedTableID, 'table-right-viewer', @RightsInsertValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			FETCH NEXT FROM RightsInsert_cursor
			INTO @RightsInsertValue
		END

		  CLOSE RightsInsert_cursor;
		  DEALLOCATE RightsInsert_cursor;

		COMMIT TRAN
	  END TRY
	  BEGIN CATCH
		IF (@@TRANCOUNT > 0)
		  ROLLBACK TRAN;

		THROW; -- raise error to the client
	  END CATCH
	END

	--------------

	IF @What = 'table' AND @Action = 'delete' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = @ID
			AND Type = 'table-right-editor'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table editor rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'table')
			AND tab_right_group.Type = 'table-group-right-editor'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table group editor rights missing.', 20, -1) WITH LOG
		END

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ID = @ID AND Type = 'table'

		DELETE FROM Config WHERE ID = @ID AND Type = 'table'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'table-comment'

		DELETE FROM Config WHERE ParentID = @ID AND Type = 'table-comment'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'table-sql-trigger'

		DELETE FROM Config WHERE ParentID = @ID AND Type = 'table-sql-trigger'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND (Type = 'table-right-designer' OR Type = 'table-right-editor' OR Type = 'table-right-viewer')

		DELETE FROM Config WHERE ParentID = @ID AND (Type = 'table-right-designer' OR Type = 'table-right-editor' OR Type = 'table-right-viewer')
	END

	--------------

	IF @What = 'table-group' AND @Action = 'insert' BEGIN
		BEGIN TRY
		BEGIN TRAN

		DECLARE @NewInsertTableGroupTitle varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		DELETE TOP (1) FROM @SplitTable

		DECLARE @NewInsertTableGroupComment varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);


		IF EXISTS (SELECT TOP 1
			Value
		FROM config
		WHERE type = 'table-group' AND value = @NewInsertTableGroupTitle) BEGIN
			DECLARE @UniqueInsertTableGroupNameError varchar(3000) = 'A table group with the name ' + @NewInsertTableGroupTitle + ' already exists.';
			SELECT 'Error'[WebAPIMessageType], @UniqueInsertTableGroupNameError[WebAPIMessage]
			RAISERROR (@UniqueInsertTableGroupNameError, 20, -1) WITH LOG
		END

		INSERT INTO Config
			(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(NULL, 'table-group', @NewInsertTableGroupTitle, @CreatedBy, @CreatedByID, GETDATE())

		DECLARE @LastInsertedGroupID int = SCOPE_IDENTITY();


		INSERT INTO Config
			(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@LastInsertedGroupID, 'table-group-comment', @NewInsertTableGroupComment, @CreatedBy, @CreatedByID, GETDATE())

		DECLARE @GroupRightsInsertValue VARCHAR(max);
		DECLARE GroupRightsInsert_cursor CURSOR FOR
		SELECT val
		FROM @SplitTable

		  OPEN GroupRightsInsert_cursor

		  FETCH NEXT FROM GroupRightsInsert_cursor
		  INTO @GroupRightsInsertValue

		  WHILE @@FETCH_STATUS = 0 BEGIN
			IF LEFT(@GroupRightsInsertValue, 1) = 'd' BEGIN
				SET @GroupRightsInsertValue = LTRIM(RTRIM(SUBSTRING(@GroupRightsInsertValue, 2, LEN(@GroupRightsInsertValue))));
				IF @GroupRightsInsertValue <> ''
				BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@LastInsertedGroupID, 'table-group-right-designer', @GroupRightsInsertValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			ELSE IF LEFT(@GroupRightsInsertValue, 1) = 'e' BEGIN
				SET @GroupRightsInsertValue = LTRIM(RTRIM(SUBSTRING(@GroupRightsInsertValue, 2, LEN(@GroupRightsInsertValue))));
				IF @GroupRightsInsertValue <> ''
				BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@LastInsertedGroupID, 'table-group-right-editor', @GroupRightsInsertValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			ELSE IF LEFT(@GroupRightsInsertValue, 1) = 'v' BEGIN
				SET @GroupRightsInsertValue = LTRIM(RTRIM(SUBSTRING(@GroupRightsInsertValue, 2, LEN(@GroupRightsInsertValue))));
				IF @GroupRightsInsertValue <> '' BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@LastInsertedGroupID, 'table-group-right-viewer', @GroupRightsInsertValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			FETCH NEXT FROM GroupRightsInsert_cursor
			INTO @GroupRightsInsertValue
		END

		  CLOSE GroupRightsInsert_cursor;
		  DEALLOCATE GroupRightsInsert_cursor;

		COMMIT TRAN
	  END TRY
	  BEGIN CATCH
		IF (@@TRANCOUNT > 0)
		  ROLLBACK TRAN;

		THROW; -- raise error to the client
	  END CATCH
	END

	--------------

	IF @What = 'table-group' AND @Action = 'update' BEGIN
		BEGIN TRY
		BEGIN TRAN

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = @ID
			AND tab_right_group.Type = 'table-group-right-designer'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table group editor rights missing.', 20, -1) WITH LOG
		END

		DECLARE @NewTableGroupTitle varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		DELETE TOP (1) FROM @SplitTable

		DECLARE @NewTableGroupComments varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		IF EXISTS (SELECT TOP 1
			Value
		FROM config
		WHERE type = 'table-group' AND value = @NewInsertTableGroupTitle AND ID <> @ID) BEGIN
			DECLARE @UniqueUpdateTableGroupNameError varchar(3000) = 'A table group with the name ' + @NewInsertTableGroupTitle + ' already exists.';
			SELECT 'Error'[WebAPIMessageType], @UniqueUpdateTableGroupNameError[WebAPIMessage]
			RAISERROR (@UniqueUpdateTableGroupNameError, 20, -1) WITH LOG
		END

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ID = @ID AND Type = 'table-group'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'table-group-comment'

		UPDATE Config
		SET value = @NewTableGroupTitle,
        ModifiedBy = @CreatedBy,
        ModifiedByID = @CreatedByID,
        ModifiedDate = GETDATE()
		WHERE ID = @ID AND Type = 'table-group'

		UPDATE Config
		SET value = @NewTableGroupComments,
        ModifiedBy = @CreatedBy,
        ModifiedByID = @CreatedByID,
        ModifiedDate = GETDATE()
		WHERE ParentID = @ID AND Type = 'table-group-comment'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND (Type = 'table-group-right-designer' OR Type = 'table-group-right-editor' OR Type = 'table-group-right-viewer')
		DELETE FROM Config
		WHERE ParentID = @ID AND (Type = 'table-group-right-designer' OR Type = 'table-group-right-editor' OR Type = 'table-group-right-viewer')

		DECLARE @GroupRightsUpdateValue VARCHAR(max);
		DECLARE GroupRightsUpdate_cursor CURSOR FOR
		SELECT val
		FROM @SplitTable

		OPEN GroupRightsUpdate_cursor
		  FETCH NEXT FROM GroupRightsUpdate_cursor
		  INTO @GroupRightsUpdateValue

		  WHILE @@FETCH_STATUS = 0 BEGIN
			IF LEFT(@GroupRightsUpdateValue, 1) = 'd' BEGIN
				SET @GroupRightsUpdateValue = LTRIM(RTRIM(SUBSTRING(@GroupRightsUpdateValue, 2, LEN(@GroupRightsUpdateValue))));
				IF @GroupRightsUpdateValue <> ''
				BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@ID, 'table-group-right-designer', @GroupRightsUpdateValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			ELSE IF LEFT(@GroupRightsUpdateValue, 1) = 'e' BEGIN
				SET @GroupRightsUpdateValue = LTRIM(RTRIM(SUBSTRING(@GroupRightsUpdateValue, 2, LEN(@GroupRightsUpdateValue))));
				IF @GroupRightsUpdateValue <> ''
				BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@ID, 'table-group-right-editor', @GroupRightsUpdateValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			ELSE IF LEFT(@GroupRightsUpdateValue, 1) = 'v' BEGIN
				SET @GroupRightsUpdateValue = LTRIM(RTRIM(SUBSTRING(@GroupRightsUpdateValue, 2, LEN(@GroupRightsUpdateValue))));
				IF @GroupRightsUpdateValue <> '' BEGIN
					INSERT INTO Config
						(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
					VALUES
						(@ID, 'table-group-right-viewer', @GroupRightsUpdateValue, @CreatedBy, @CreatedByID, GETDATE())
				END
			END
			FETCH NEXT FROM GroupRightsUpdate_cursor
			INTO @GroupRightsUpdateValue
		END

		CLOSE GroupRightsUpdate_cursor;
		DEALLOCATE GroupRightsUpdate_cursor;

		COMMIT TRAN
	  END TRY
	  BEGIN CATCH
		IF (@@TRANCOUNT > 0)
		  ROLLBACK TRAN;

		THROW; -- raise error to the client
	  END CATCH
	END

	--------------

	IF @What = 'table-group' AND @Action = 'delete' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = @ID
			AND tab_right_group.Type = 'table-group-right-designer'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table group editor rights missing.', 20, -1) WITH LOG
		END

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ID = @ID AND Type = 'table-group'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'table-group-comment'

		DELETE FROM Config WHERE ID = @ID AND Type = 'table-group'

		DELETE FROM Config WHERE ParentID = @ID AND Type = 'table-group-comment'
	END

	--------------

	IF @What = 'field' AND @Action = 'insert' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = @ID
			AND Type = 'table-right-designer'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN

			SELECT 'Error'[WebAPIMessageType], 'Table designer rights missing.'[WebAPIMessage]
			RAISERROR ('Table designer rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'table')
			AND tab_right_group.Type = 'table-group-right-designer'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group designer rights missing.'[WebAPIMessage]
			RAISERROR ('Table group designer rights missing.', 20, -1) WITH LOG
		END

		DECLARE @NextFieldOrderNumber int = (SELECT
			(MAX(ISNULL(field_display_order.DataLinkID, 0) + 1))
		FROM Config selected_table
			LEFT OUTER JOIN Config field
			ON selected_table.id = field.ParentID
				AND field.Type = 'field'
			LEFT OUTER JOIN Config field_display_order
			ON field_display_order.ParentID = field.id
				AND field_display_order.Type = 'field-display-order'
		WHERE selected_table.type = 'table'
			AND selected_table.ID = @ID)

		DECLARE @InsertFieldTableID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @InsertFieldTypeID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @InsertFieldTypeSystemName varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		IF @InsertFieldTypeSystemName = 'choice' BEGIN
			DECLARE @InsertFieldChoiceList varchar(max) = (SELECT TOP 1
				RTRIM(LTRIM(val))
			FROM @SplitTable);
			DELETE TOP (1) FROM @SplitTable
		END
		DECLARE @InsertFieldTitle varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DECLARE @InsertFieldMultiLookup bit = 0;
		IF LTRIM(RTRIM(ISNULL(@InsertFieldTitle,''))) = '__[[{{multi-code-selection}}]]__' BEGIN
			SET @InsertFieldMultiLookup = 1;
			DELETE TOP (1) FROM @SplitTable
			SET @InsertFieldTitle = (SELECT TOP 1
				RTRIM(LTRIM(val))
			FROM @SplitTable);
		END
		DELETE TOP (1) FROM @SplitTable
		DECLARE @InsertFieldTooltopText varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @InsertFieldUniquenessID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @InsertFieldRequiredID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @InsertFieldHideOnInsert varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		SET @InsertFieldTableID = NULLIF(@InsertFieldTableID, '')
		SET @InsertFieldTypeID = NULLIF(@InsertFieldTypeID, '')
		SET @InsertFieldUniquenessID = NULLIF(@InsertFieldUniquenessID, '')
		SET @InsertFieldRequiredID = NULLIF(@InsertFieldRequiredID, '')
		SET @InsertFieldHideOnInsert = NULLIF(@InsertFieldHideOnInsert, '')

		IF EXISTS (SELECT TOP 1
			Value
		FROM config
		WHERE type = 'field'
			AND parentid = @ID
			AND value = @InsertFieldTitle) BEGIN
			DECLARE @UniqueInsertFieldName varchar(max) = 'A field with the name ' + @InsertFieldTitle + ' already exists.';
			SELECT 'Error'[WebAPIMessageType], @UniqueInsertFieldName[WebAPIMessage]
			RAISERROR (@UniqueInsertFieldName, 20, -1) WITH LOG
		END

		INSERT INTO Config
			(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@InsertFieldTableID, 'field', @InsertFieldTitle, @CreatedBy, @CreatedByID, GETDATE())

		DECLARE @InsertFieldNewID int = SCOPE_IDENTITY();

		INSERT INTO Config
			(ParentID, Type, DataLinkID, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@InsertFieldNewID, 'field-type', @InsertFieldTypeID, @CreatedBy, @CreatedByID, GETDATE())

		IF LEN(LTRIM(RTRIM(ISNULL(@InsertFieldChoiceList,'')))) > 0 AND @InsertFieldTypeSystemName = 'choice' BEGIN
			INSERT INTO Config
				(ParentID, Value, Type, CreatedBy, CreatedByID, CreatedDate)
			VALUES
				(@InsertFieldNewID, @InsertFieldChoiceList, 'field-choices', @CreatedBy, @CreatedByID, GETDATE())
		END

		IF @InsertFieldMultiLookup = 1 BEGIN
			INSERT INTO Config
				(ParentID, Value, Type, CreatedBy, CreatedByID, CreatedDate)
			VALUES
				(@InsertFieldNewID, 'Y', 'field-multi-selection', @CreatedBy, @CreatedByID, GETDATE())
		END

		INSERT INTO Config
			(ParentID, Type, DataLinkID, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@InsertFieldNewID, 'field-display-order', @NextFieldOrderNumber, @CreatedBy, @CreatedByID, GETDATE())

		INSERT INTO Config
			(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@InsertFieldNewID, 'field-tooltip', @InsertFieldTooltopText, @CreatedBy, @CreatedByID, GETDATE())

		INSERT INTO Config
			(ParentID, Type, DataLinkID, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@InsertFieldNewID, 'field-uniqueness', @InsertFieldUniquenessID, @CreatedBy, @CreatedByID, GETDATE())

		INSERT INTO Config
			(ParentID, Type, DataLinkID, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@InsertFieldNewID, 'field-required-type', @InsertFieldRequiredID, @CreatedBy, @CreatedByID, GETDATE())

		INSERT INTO Config
			(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@InsertFieldNewID, 'field-hide-on-insert', @InsertFieldHideOnInsert, @CreatedBy, @CreatedByID, GETDATE())
	END

	--------------

	IF @What = 'field' AND @Action = 'update' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'Field')
			AND Type = 'table-right-designer'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table designer rights missing.'[WebAPIMessage]
			RAISERROR ('Table designer rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = ((SELECT ParentID
			FROM Config
			WHERE ID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'Field')))
			AND tab_right_group.Type = 'table-group-right-designer'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group designer rights missing.'[WebAPIMessage]
			RAISERROR ('Table group designer rights missing.', 20, -1) WITH LOG
		END

		DECLARE @UpdateFieldID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @UpdateFieldTypeID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @UpdateFieldTypeSystemName varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		IF @UpdateFieldTypeSystemName = 'choice' BEGIN
			DECLARE @UpdateFieldChoiceList varchar(max) = (SELECT TOP 1
				RTRIM(LTRIM(val))
			FROM @SplitTable);
			DELETE TOP (1) FROM @SplitTable
		END
		DECLARE @UpdateFieldTitle varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DECLARE @UpdateFieldMultiLookup bit = 0;
		IF LTRIM(RTRIM(ISNULL(@UpdateFieldTitle,''))) = '__[[{{multi-code-selection}}]]__' BEGIN
			SET @UpdateFieldMultiLookup = 1;
			DELETE TOP (1) FROM @SplitTable
			SET @UpdateFieldTitle = (SELECT TOP 1
				RTRIM(LTRIM(val))
			FROM @SplitTable);
		END
		DELETE TOP (1) FROM @SplitTable
		DECLARE @UpdateFieldTooltopText varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @UpdateFieldUniquenessID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @UpdateFieldRequiredID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @UpdateFieldHideOnInsert varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		SET @UpdateFieldTypeID = NULLIF(@UpdateFieldTypeID, '')
		SET @UpdateFieldUniquenessID = NULLIF(@UpdateFieldUniquenessID, '')
		SET @UpdateFieldRequiredID = NULLIF(@UpdateFieldRequiredID, '')
		SET @UpdateFieldHideOnInsert = NULLIF(@UpdateFieldHideOnInsert, '')

		IF EXISTS (SELECT TOP 1
			Value
		FROM config
		WHERE type = 'field'
			AND value = @UpdateFieldTitle
			AND ID <> @ID) BEGIN
			DECLARE @UniqueUpdateFieldNameError varchar(3000) = 'A field with the name ' + @UpdateFieldTitle + ' already exists.';
			SELECT 'Error'[WebAPIMessageType], @UniqueUpdateFieldNameError[WebAPIMessage]
			RAISERROR (@UniqueUpdateFieldNameError, 20, -1) WITH LOG
		END

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ID = @ID AND Type = 'field'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'field-type'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'field-tooltip'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'field-uniqueness'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'field-required-type'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'field-hide-on-insert'

		UPDATE Config SET value = @UpdateFieldTitle,
        ModifiedBy = @CreatedBy,
        ModifiedByID = @CreatedByID,
        ModifiedDate = GETDATE() WHERE ID = @ID AND Type = 'field'

		UPDATE Config SET DataLinkID = @UpdateFieldTypeID,
        ModifiedBy = @CreatedBy,
        ModifiedByID = @CreatedByID,
        ModifiedDate = GETDATE() WHERE ParentID = @ID AND Type = 'field-type'

		UPDATE Config SET Value = @UpdateFieldTooltopText,
        CreatedBy = @CreatedBy,
        CreatedByID = @CreatedByID,
        CreatedDate = GETDATE() WHERE ParentID = @ID AND Type = 'field-tooltip'

		UPDATE Config SET DataLinkID = @UpdateFieldUniquenessID,
        ModifiedBy = @CreatedBy,
        ModifiedByID = @CreatedByID,
        ModifiedDate = GETDATE()
		WHERE ParentID = @ID AND Type = 'field-uniqueness'

		UPDATE Config SET DataLinkID = @UpdateFieldRequiredID,
        ModifiedBy = @CreatedBy,
        ModifiedByID = @CreatedByID,
        ModifiedDate = GETDATE()
		WHERE ParentID = @ID AND Type = 'field-required-type'

		UPDATE Config SET Value = @UpdateFieldHideOnInsert,
        ModifiedBy = @CreatedBy,
        ModifiedByID = @CreatedByID,
        ModifiedDate = GETDATE()
		WHERE ParentID = @ID AND Type = 'field-hide-on-insert'
	END

	--------------

	IF @What = 'field' AND @Action = 'swap-order' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'Field')
			AND Type = 'table-right-designer'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table designer rights missing.'[WebAPIMessage]
			RAISERROR ('Table designer rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = ((SELECT ParentID
			FROM Config
			WHERE ID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'Field')))
			AND tab_right_group.Type = 'table-group-right-designer'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group designer rights missing.'[WebAPIMessage]
			RAISERROR ('Table group designer rights missing.', 20, -1) WITH LOG
		END
		DECLARE @ThisRowOrder int = (SELECT TOP 1
			DataLinkID
		FROM Config
		WHERE ParentID = @ID AND type = 'field-display-order');
		DECLARE @OtherRowOrder int = (SELECT TOP 1
			DataLinkID
		FROM Config
		WHERE ParentID = @Value AND type = 'field-display-order');

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'field-display-order'

		UPDATE Config SET DataLinkID = @OtherRowOrder,
		ModifiedBy = @CreatedBy,
        ModifiedByID = @CreatedByID,
        ModifiedDate = GETDATE()
		WHERE ParentID = @ID AND Type = 'field-display-order'

		UPDATE Config SET DataLinkID = @ThisRowOrder,
        ModifiedBy = @CreatedBy,
        ModifiedByID = @CreatedByID,
        ModifiedDate = GETDATE()
		WHERE ParentID = @Value AND Type = 'field-display-order'
	END

	--------------

	IF @What = 'field' AND @Action = 'delete' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'Field')
			AND Type = 'table-right-designer'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN
			RAISERROR ('Table designer rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'Field'))
			AND tab_right_group.Type = 'table-group-right-designer'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group designer rights missing.'[WebAPIMessage]
			RAISERROR ('Table group designer rights missing.', 20, -1) WITH LOG
		END
		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ID = @ID AND Type = 'field'

		DELETE FROM Config WHERE id = @ID AND type = 'field'
	END

	--------------

	IF @What = 'admin' AND @Action = 'insert' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE
			Type='system-admin' and Value = @CreatedBy) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'System admin right missing.'[WebAPIMessage]
			RAISERROR ('System admin right missing.', 20, -1) WITH LOG
		END

		DECLARE @InsertAdminParentID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @InsertAdminType varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @InsertAdminValue varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @InsertAdminDataLinkID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @InsertAdminStatus varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		SET @InsertAdminParentID = LTRIM(RTRIM(NULLIF(@InsertAdminParentID, '')))
		SET @InsertAdminType = LTRIM(RTRIM(NULLIF(@InsertAdminType, '')))
		SET @InsertAdminValue = LTRIM(RTRIM(NULLIF(@InsertAdminValue, '')))
		SET @InsertAdminDataLinkID = LTRIM(RTRIM(NULLIF(@UpdateFieldTypeID, '')))
		SET @InsertAdminStatus = LTRIM(RTRIM(NULLIF(@InsertAdminStatus, '')))

		INSERT INTO Config
			(ParentID, Type, Value, DataLinkID, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@InsertAdminParentID, @InsertAdminType, @InsertAdminValue, @InsertAdminDataLinkID, @CreatedBy, @CreatedByID, GETDATE())
	END

	--------------

	IF @What = 'admin' AND @Action = 'update' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE
			Type='system-admin' and Value = @CreatedBy) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'System admin right missing.'[WebAPIMessage]
			RAISERROR ('System admin right missing.', 20, -1) WITH LOG
		END

		DECLARE @UpdateAdminParentID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @UpdateAdminType varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @UpdateAdminValue varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @UpdateAdminDataLinkID varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @UpdateAdminStatus varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		SET @UpdateAdminParentID = LTRIM(RTRIM(NULLIF(@UpdateAdminParentID, '')))
		SET @UpdateAdminType = LTRIM(RTRIM(NULLIF(@UpdateAdminType, '')))
		SET @UpdateAdminValue = LTRIM(RTRIM(NULLIF(@UpdateAdminValue, '')))
		SET @UpdateAdminDataLinkID = LTRIM(RTRIM(NULLIF(@UpdateAdminDataLinkID, '')))
		SET @UpdateAdminStatus = LTRIM(RTRIM(NULLIF(@UpdateAdminStatus, '')))

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ID = @ID

		UPDATE Config SET ParentID = @UpdateAdminParentID,
        Type = @UpdateAdminType,
        Value = @UpdateAdminValue,
        DataLinkID = @UpdateAdminDataLinkID,
        ModifiedBy = @CreatedBy,
        ModifiedByID = @CreatedByID,
        ModifiedDate = GETDATE()
		WHERE ID = @ID
	END

	--------------

	IF @What = 'admin' AND @Action = 'delete' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE
			Type='system-admin' and Value = @CreatedBy) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'System admin right missing.'[WebAPIMessage]
			RAISERROR ('System admin right missing.', 20, -1) WITH LOG
		END

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ID = @ID

		DELETE FROM Config WHERE id = @ID
	END





	--------------

	IF @What = 'data' AND @Action = 'insert' BEGIN
		BEGIN TRY
		BEGIN TRAN

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = @ID
			AND Type = 'table-right-editor'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table editor rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'table')
			AND tab_right_group.Type = 'table-group-right-editor'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table group editor rights missing.', 20, -1) WITH LOG
		END

		DECLARE @DataInsertFieldID int, @DataInsertValue varchar(max),
			@InsertType varchar(20);

		DECLARE @NextTableRow int = (SELECT(MAX(ISNULL(RowNumber, 0) + 1)) [NextRow]
		FROM DataValues
		WHERE TableID = @ID)

		INSERT INTO DataValues
			(RowNumber, TableID, CreatedBy, CreatedByID, Type)
		VALUES
			(@NextTableRow, @ID, @CreatedBy, @CreatedByID, 'row')

		DECLARE @InsertRowNewID int = SCOPE_IDENTITY();

		DECLARE DataInsert_cursor CURSOR FOR
		SELECT val
		FROM @SplitTable

		OPEN DataInsert_cursor

		FETCH NEXT FROM DataInsert_cursor
		INTO @DataInsertValue

		WHILE @@FETCH_STATUS = 0 BEGIN
			SET @DataInsertFieldID = LEFT(@DataInsertValue, 8);
			SET @DataInsertValue = SUBSTRING(@DataInsertValue, 9, LEN(@DataInsertValue));

			IF @DataInsertValue like '[[][[]{{system-sql-lookup}}]]%' BEGIN
				SET @InsertType = 'value-lookup';
				SET @DataInsertValue = substring(@DataInsertValue,26,len(@DataInsertValue))
			END
			ELSE BEGIN
				SET @InsertType = 'value';
				DECLARE @NameOfRequiredFieldError VARCHAR(50) = (SELECT
					field.value
				FROM [TableBuilder].[dbo].[Config] field
				where field.Type = 'field' AND field.id = @DataInsertFieldID)
				IF EXISTS (SELECT
					list_item_field_required_type.value
				FROM [TableBuilder].[dbo].[Config] field
					left outer join Config field_required_type
					on field.ID = field_required_type.ParentID and field_required_type.Type = 'field-required-type'
					left outer join Config list_item_field_required_type
					on field_required_type.DataLinkID= list_item_field_required_type.id and list_item_field_required_type.Type = 'list-item-field-required-type'
				where field.Type = 'field'
					AND list_item_field_required_type.value = 'Required'
					AND field.ID = @DataInsertFieldID) BEGIN
					IF LEN(@DataInsertValue) = 0 BEGIN
						SET @NameOfRequiredFieldError = 'The required field ' + @NameOfRequiredFieldError + ' is missing.';
						ROLLBACK TRAN
						SELECT 'Error'[WebAPIMessageType], @NameOfRequiredFieldError[WebAPIMessage]
						RAISERROR (@NameOfRequiredFieldError, 20, -1) WITH LOG
					END
				END
				IF EXISTS (SELECT
					list_item_field_uniqueness.Value
				FROM [TableBuilder].[dbo].[Config] field
					left outer join Config field_uniqueness
					on field.ID = field_uniqueness.ParentID and field_uniqueness.Type = 'field-uniqueness'
					left outer join Config list_item_field_uniqueness
					on field_uniqueness.DataLinkID = list_item_field_uniqueness.id and list_item_field_uniqueness.Type = 'list-item-field-uniqueness'
				where field.Type = 'field'
					AND list_item_field_uniqueness.Value = 'Individually Unique'
					AND field.ID = @DataInsertFieldID) BEGIN
					IF EXISTS (SELECT val.ID
					FROM DataValues val
						left outer join BigDataValues big_data on val.bigdatavalueid = big_data.id
						left outer join VeryBigDataValues very_big_data on val.verybigdatavalueid = very_big_data.id
					WHERE 
					val.Type='value'
						AND val.FieldID =  @DataInsertFieldID
						AND LTRIM(RTRIM(ISNULL(val.Value, ''))) + LTRIM(RTRIM(ISNULL(big_data.Value, ''))) + LTRIM(RTRIM(ISNULL(very_big_data.Value, ''))) = @DataInsertValue) BEGIN
						SET @NameOfRequiredFieldError = 'The field ' + @NameOfRequiredFieldError + ' must be unique.';
						ROLLBACK TRAN
						SELECT 'Error'[WebAPIMessageType], @NameOfRequiredFieldError[WebAPIMessage]
						RAISERROR (@NameOfRequiredFieldError, 20, -1) WITH LOG
					END
				END
			END

			IF LEN(@DataInsertValue) > 8000 AND @DataInsertFieldID > 0 BEGIN
				INSERT INTO VeryBigDataValues
					(Value)
				VALUES
					(@DataInsertValue)

				INSERT INTO DataValues
					(Value, TableID, RowID, FieldID, RowNumber, VeryBigDataValueID, CreatedBy, CreatedByID, Type)
				VALUES
					(@DataInsertValue, @ID, @InsertRowNewID, @DataInsertFieldID, @NextTableRow, SCOPE_IDENTITY(), @CreatedBy, @CreatedByID, @InsertType);
			END

			IF LEN(@DataInsertValue) > 3000 AND @DataInsertFieldID > 0 BEGIN
				INSERT INTO BigDataValues
					(Value)
				VALUES
					(@DataInsertValue)

				INSERT INTO DataValues
					(Value, TableID, RowID, FieldID, RowNumber, BigDataValueID, CreatedBy, CreatedByID, Type)
				VALUES
					(@DataInsertValue, @ID, @InsertRowNewID, @DataInsertFieldID, @NextTableRow, SCOPE_IDENTITY(), @CreatedBy, @CreatedByID, @InsertType);
			END

			IF LEN(@DataInsertValue) <= 3000 AND @DataInsertFieldID > 0 BEGIN
				INSERT INTO DataValues
					(Value, TableID, RowID, FieldID, RowNumber, CreatedBy, CreatedByID, Type)
				VALUES
					(@DataInsertValue, @ID, @InsertRowNewID, @DataInsertFieldID, @NextTableRow, @CreatedBy, @CreatedByID, @InsertType);
			END

			FETCH NEXT FROM DataInsert_cursor
        INTO @DataInsertValue
		END
      CLOSE DataInsert_cursor;
      DEALLOCATE DataInsert_cursor;

    COMMIT TRAN
  END TRY
  BEGIN CATCH
    IF (@@TRANCOUNT > 0)
      ROLLBACK TRAN;

    THROW; -- raise error to the client
  END CATCH
	END

	--------------

	IF @What = 'data' AND @Action = 'update' BEGIN
		BEGIN TRY
		BEGIN TRAN

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = (SELECT TableID
			FROM DataValues
			WHERE ID = @ID AND Type = 'Value')
			AND Type = 'table-right-editor'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table editor rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = (SELECT TableID
			FROM DataValues
			WHERE ID = @ID AND Type = 'Value'))
			AND tab_right_group.Type = 'table-group-right-editor'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table group editor rights missing.', 20, -1) WITH LOG
		END

		DECLARE @DataUpdateFieldID int,
            @DataUpdateValue varchar(max),
            @UpdateValueID int = 0,
			@UpdateBigValueID int = 0,
			@UpdateVeryBigValueID int = 0,
			@UpdateType varchar(20);

		INSERT INTO DataValuesArchive
			(OriginalID,TableID,RowID,FieldID,RowNumber,Type,Value,BigDataValueID,VeryBigDataValueID,
			CreatedBy,CreatedByID,CreatedDate,
			ModifiedBy,ModifiedByID,ModifiedDate,
			ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT ID, TableID, RowID, FieldID, RowNumber, Type, Value, BigDataValueID, VeryBigDataValueID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM DataValues
		WHERE ID = @ID AND type = 'row'

	-- archive all the values for the row
		INSERT INTO DataValuesArchive
			(OriginalID,TableID,RowID,FieldID,RowNumber,Type,Value,BigDataValueID,VeryBigDataValueID,
			CreatedBy,CreatedByID,CreatedDate,
			ModifiedBy,ModifiedByID,ModifiedDate,
			ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT ID, TableID, RowID, FieldID, RowNumber, Type, Value, BigDataValueID, VeryBigDataValueID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM DataValues
		WHERE RowID = @ID AND type = 'value'

		INSERT INTO BigDataValuesArchive
			(OriginalID, Value,ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT big.id, big.Value, @CreatedBy, @CreatedByID, GETDATE()
		FROM DataValues vals
			left outer join BigDataValues big on vals.BigDataValueID = big.id
		WHERE vals.rowID = @ID AND vals.type = 'value'

		INSERT INTO VeryBigDataValuesArchive
			(OriginalID, Value,ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT big.id, big.Value, @CreatedBy, @CreatedByID, GETDATE()
		FROM DataValues vals
			left outer join VeryBigDataValues big on vals.VeryBigDataValueID = big.id
		WHERE vals.rowID = @ID AND vals.type = 'value'

    DECLARE DataUpdate_cursor CURSOR FOR
    SELECT
			val
		FROM @SplitTable

    OPEN DataUpdate_cursor

    FETCH NEXT FROM DataUpdate_cursor
    INTO @DataUpdateValue

    WHILE @@FETCH_STATUS = 0 BEGIN
			SET @DataUpdateFieldID = LEFT(@DataUpdateValue, 8);
			SET @DataUpdateValue = LTRIM(RTRIM(SUBSTRING(@DataUpdateValue, 9, LEN(@DataUpdateValue))));
			SET @UpdateValueID = (SELECT TOP 1
				id
			FROM DataValues
			WHERE type = 'value' AND RowID = @ID AND FieldID = @DataUpdateFieldID)
			SET @UpdateBigValueID = (SELECT TOP 1
				BigDataValueID
			FROM DataValues
			WHERE type = 'value' AND RowID = @ID AND FieldID = @DataUpdateFieldID)
			SET @UpdateVeryBigValueID = (SELECT TOP 1
				VeryBigDataValueID
			FROM DataValues
			WHERE type = 'value' AND RowID = @ID AND FieldID = @DataUpdateFieldID)

			IF @DataUpdateValue like '[[][[]{{system-sql-lookup}}]]%' BEGIN
				SET @UpdateType = 'value-lookup';
				SET @DataUpdateValue = substring(@DataUpdateValue,26,len(@DataUpdateValue))
				SET @UpdateValueID = (SELECT TOP 1
					id
				FROM DataValues
				WHERE type = 'value-lookup' AND RowID = @ID AND FieldID = @DataUpdateFieldID)
				SET @UpdateBigValueID = (SELECT TOP 1
					BigDataValueID
				FROM DataValues
				WHERE type = 'value-lookup' AND RowID = @ID AND FieldID = @DataUpdateFieldID)
				SET @UpdateVeryBigValueID = (SELECT TOP 1
					VeryBigDataValueID
				FROM DataValues
				WHERE type = 'value-lookup' AND RowID = @ID AND FieldID = @DataUpdateFieldID)
			END
			ELSE BEGIN
				SET @UpdateType = 'value';
				DECLARE @NameOfRequiredFieldUpdateError VARCHAR(50) = (SELECT
					field.value
				FROM [TableBuilder].[dbo].[Config] field
				where field.Type = 'field' AND field.id = @DataUpdateFieldID)
				IF EXISTS (SELECT
					list_item_field_required_type.value
				FROM [TableBuilder].[dbo].[Config] field
					left outer join Config field_required_type
					on field.ID = field_required_type.ParentID and field_required_type.Type = 'field-required-type'
					left outer join Config list_item_field_required_type
					on field_required_type.DataLinkID= list_item_field_required_type.id and list_item_field_required_type.Type = 'list-item-field-required-type'
				where field.Type = 'field'
					AND list_item_field_required_type.value = 'Required'
					AND field.ID = @DataUpdateFieldID) BEGIN
					IF LEN(@DataUpdateValue) =0 BEGIN
						SET @NameOfRequiredFieldUpdateError = 'The required field ' + @NameOfRequiredFieldUpdateError + ' is missing.';
						ROLLBACK TRAN
						SELECT 'Error'[WebAPIMessageType], @NameOfRequiredFieldUpdateError[WebAPIMessage]
						RAISERROR (@NameOfRequiredFieldUpdateError, 20, -1) WITH LOG
					END
				END
				IF EXISTS (SELECT
					list_item_field_uniqueness.Value
				FROM [TableBuilder].[dbo].[Config] field
					left outer join Config field_uniqueness
					on field.ID = field_uniqueness.ParentID and field_uniqueness.Type = 'field-uniqueness'
					left outer join Config list_item_field_uniqueness
					on field_uniqueness.DataLinkID = list_item_field_uniqueness.id and list_item_field_uniqueness.Type = 'list-item-field-uniqueness'
				where field.Type = 'field'
					AND list_item_field_uniqueness.Value = 'Individually Unique'
					AND field.ID = @DataUpdateFieldID) BEGIN
					IF EXISTS (SELECT val.ID
					FROM DataValues val
						left outer join BigDataValues big_data on val.bigdatavalueid = big_data.id
						left outer join VeryBigDataValues very_big_data on val.verybigdatavalueid = very_big_data.id
					WHERE 
					val.Type='value'
						AND val.FieldID =  @DataUpdateFieldID
						AND val.rowid <> @ID
						AND LTRIM(RTRIM(ISNULL(val.Value, ''))) + LTRIM(RTRIM(ISNULL(big_data.Value, ''))) + LTRIM(RTRIM(ISNULL(very_big_data.Value, ''))) = @DataUpdateValue) BEGIN
						SET @NameOfRequiredFieldUpdateError = 'The field ' + @NameOfRequiredFieldUpdateError + ' must be unique.';
						ROLLBACK TRAN
						SELECT 'Error'[WebAPIMessageType], @NameOfRequiredFieldUpdateError[WebAPIMessage]
						RAISERROR (@NameOfRequiredFieldUpdateError, 20, -1) WITH LOG
					END
				END
			END

			IF @UpdateBigValueID IS NOT NULL BEGIN
				DELETE FROM BigDataValues
			WHERE ID = @UpdateBigValueID
			END

			IF @UpdateVeryBigValueID IS NOT NULL BEGIN
				DELETE FROM VeryBigDataValues
			WHERE ID = @UpdateVeryBigValueID
			END

			IF LEN(@DataUpdateValue) > 8000 AND @DataUpdateFieldID > 0 BEGIN
				INSERT INTO VeryBigDataValues
					(Value)
				Values
					(@DataUpdateValue)

				IF EXISTS (SELECT *
				FROM DataValues
				WHERE RowID = @ID AND type = @UpdateType AND FieldID = @DataUpdateFieldID) BEGIN
					UPDATE DataValues
					SET Value = NULL,
					BigDataValueID = NULL,
					VeryBigDataValueID = SCOPE_IDENTITY(),
					ModifiedBy = @CreatedBy,
					ModifiedByID = @CreatedByID,
					ModifiedDate = GETDATE()
					WHERE ID = @UpdateValueID
				END
				IF NOT EXISTS (SELECT *
				FROM DataValues
				WHERE RowID = @ID AND type = @UpdateType AND FieldID = @DataUpdateFieldID) BEGIN
					DECLARE @TableIDForNewVeryBigField VARCHAR(50) = (SELECT TOP 1
						TableID
					FROM DataValues
					WHERE RowID = @ID AND TableID IS NOT NULL)
					DECLARE @RowNumberForNewVeryBigField VARCHAR(50) = (SELECT TOP 1
						RowNumber
					FROM DataValues
					WHERE RowID = @ID AND TableID = @TableIDForNewVeryBigField)
					INSERT INTO DataValues
						(VeryBigDataValueID, TableID, RowID, FieldID, RowNumber, CreatedBy, CreatedByID, Type)
					VALUES
						(SCOPE_IDENTITY(), @TableIDForNewVeryBigField, @ID, @DataUpdateFieldID, @RowNumberForNewVeryBigField, @CreatedBy, @CreatedByID, @UpdateType);
				END
			END

			IF LEN(@DataUpdateValue) > 3000 AND @DataUpdateFieldID > 0 BEGIN
				INSERT INTO BigDataValues
					(Value)
				Values
					(@DataUpdateValue)

				IF EXISTS (SELECT *
				FROM DataValues
				WHERE RowID = @ID AND type = @UpdateType AND FieldID = @DataUpdateFieldID) BEGIN
					UPDATE DataValues
				SET Value = NULL,
					BigDataValueID = SCOPE_IDENTITY(),
					VeryBigDataValueID = NULL,
					ModifiedBy = @CreatedBy,
					ModifiedByID = @CreatedByID,
					ModifiedDate = GETDATE()
				WHERE RowID = @ID
						AND type = @UpdateType
						AND FieldID = @DataUpdateFieldID
				END
				IF NOT EXISTS (SELECT *
				FROM DataValues
				WHERE RowID = @ID AND type = @UpdateType AND FieldID = @DataUpdateFieldID) BEGIN
					DECLARE @TableIDForNewBigField VARCHAR(50) = (SELECT TOP 1
						TableID
					FROM DataValues
					WHERE RowID = @ID AND TableID IS NOT NULL)
					DECLARE @RowNumberForNewBigField VARCHAR(50) = (SELECT TOP 1
						RowNumber
					FROM DataValues
					WHERE RowID = @ID AND TableID = @TableIDForNewBigField)
					INSERT INTO DataValues
						(BigDataValueID, TableID, RowID, FieldID, RowNumber, CreatedBy, CreatedByID, Type)
					VALUES
						(SCOPE_IDENTITY(), @TableIDForNewBigField, @ID, @DataUpdateFieldID, @RowNumberForNewBigField, @CreatedBy, @CreatedByID, @UpdateType);
				END
			END

			IF LEN(@DataUpdateValue) <= 3000 AND @DataUpdateFieldID > 0 BEGIN
				IF EXISTS (SELECT *
				FROM DataValues
				WHERE RowID = @ID AND type = @UpdateType AND FieldID = @DataUpdateFieldID) BEGIN
					UPDATE DataValues
				SET Value = @DataUpdateValue,
					BigDataValueID = NULL,
					VeryBigDataValueID = NULL,
					ModifiedBy = @CreatedBy,
					ModifiedByID = @CreatedByID,
					ModifiedDate = GETDATE()
				WHERE RowID = @ID
						AND type = @UpdateType
						AND FieldID = @DataUpdateFieldID
				END
				IF NOT EXISTS (SELECT *
				FROM DataValues
				WHERE RowID = @ID AND type = @UpdateType AND FieldID = @DataUpdateFieldID) BEGIN
					DECLARE @TableIDForNewField VARCHAR(50) = (SELECT TOP 1
						TableID
					FROM DataValues
					WHERE RowID = @ID AND TableID IS NOT NULL)
					DECLARE @RowNumberForNewField VARCHAR(50) = (SELECT TOP 1
						RowNumber
					FROM DataValues
					WHERE RowID = @ID AND TableID = @TableIDForNewField)
					INSERT INTO DataValues
						(Value, TableID, RowID, FieldID, RowNumber, CreatedBy, CreatedByID, Type)
					VALUES
						(@DataUpdateValue, @TableIDForNewField, @ID, @DataUpdateFieldID, @RowNumberForNewField, @CreatedBy, @CreatedByID, @UpdateType);
				END
			END
			FETCH NEXT FROM DataUpdate_cursor
		INTO @DataUpdateValue
		END
    CLOSE DataUpdate_cursor;
    DEALLOCATE DataUpdate_cursor;
    COMMIT TRAN
	END TRY
	BEGIN CATCH
    IF (@@TRANCOUNT > 0)
		ROLLBACK TRAN;

		THROW; -- raise error to the client
	END CATCH
	END

	--------------

	IF @What = 'data' AND @Action = 'delete' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = (SELECT TableID
			FROM DataValues
			WHERE ID = @ID AND Type = 'Value')
			AND Type = 'table-right-editor'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table editor rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = (SELECT TableID
			FROM DataValues
			WHERE ID = @ID AND Type = 'Value'))
			AND tab_right_group.Type = 'table-group-right-editor'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table group editor rights missing.', 20, -1) WITH LOG
		END

		DECLARE @DataDeleteFieldID int, @DataDeleteValue varchar(max), @DeleteValueID int = 0;

		INSERT INTO DataValuesArchive
			(OriginalID,TableID,RowID,FieldID,RowNumber,Type,Value,BigDataValueID,VeryBigDataValueID,
			CreatedBy,CreatedByID,CreatedDate,
			ModifiedBy,ModifiedByID,ModifiedDate,
			ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT ID, TableID, RowID, FieldID, RowNumber, Type, Value, BigDataValueID, VeryBigDataValueID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM DataValues
		WHERE ID = @ID AND type = 'row'

		-- archive all the values for the row
		INSERT INTO DataValuesArchive
			(OriginalID,TableID,RowID,FieldID,RowNumber,Type,Value,BigDataValueID,VeryBigDataValueID,
			CreatedBy,CreatedByID,CreatedDate,
			ModifiedBy,ModifiedByID,ModifiedDate,
			ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT ID, TableID, RowID, FieldID, RowNumber, Type, Value, BigDataValueID, VeryBigDataValueID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM DataValues
		WHERE RowID = @ID AND type = 'value'

		INSERT INTO BigDataValuesArchive
			(OriginalID, Value,ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT big.id, big.Value, @CreatedBy, @CreatedByID, GETDATE()
		FROM DataValues vals
			left outer join BigDataValues big on vals.BigDataValueID = big.id
		WHERE vals.rowID = @ID AND vals.type = 'value'

		DELETE FROM BigDataValues WHERE ID IN (
		  SELECT big.id
		FROM DataValues vals
			left outer join BigDataValues big on vals.BigDataValueID = big.id
		WHERE vals.rowID = @ID AND vals.type = 'value')

		INSERT INTO VeryBigDataValuesArchive
			(OriginalID, Value,ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT big.id, big.Value, @CreatedBy, @CreatedByID, GETDATE()
		FROM DataValues vals
			left outer join VeryBigDataValues big on vals.VeryBigDataValueID = big.id
		WHERE vals.rowID = @ID AND vals.type = 'value'

		DELETE FROM VeryBigDataValues WHERE ID IN (
		  SELECT big.id
		FROM DataValues vals
			left outer join VeryBigDataValues big on vals.VeryBigDataValueID = big.id
		WHERE vals.rowID = @ID AND vals.type = 'value')

		DELETE FROM DataValues WHERE ID = @ID AND type = 'row'
		DELETE FROM DataValues WHERE RowID = @ID AND type = 'value'
	END


	--------------


	IF @What = 'sql-report-subscription' AND @Action = 'insert' BEGIN
		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE [type] = 'sql-report-subscription'
			AND CreatedBy = @CreatedBy
			AND ParentID = @ID

		DELETE FROM Config WHERE [type] = 'sql-report-subscription'
			AND CreatedBy = @CreatedBy
			AND ParentID = @ID

		IF @Value <> '516' BEGIN
			-- None
			INSERT INTO config
				(ParentID, type, DataLinkID, CreatedBy, CreatedByID, CreatedDate)
			VALUES
				(@ID, 'sql-report-subscription', @Value, @CreatedBy, @CreatedByID, GETDATE())
		END
	END

	--------------

	IF @What = 'data-view' AND @Action = 'insert' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = @ID
			AND Type = 'table-right-designer'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table designer rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'table')
			AND tab_right_group.Type = 'table-group-right-designer'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group editor rights missing.'[WebAPIMessage]
			RAISERROR ('Table group designer rights missing.', 20, -1) WITH LOG
		END

		DECLARE @NewInsertDataViewTitle varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @NewInsertDataViewComment varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @NewInsertDataViewSQL varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		IF EXISTS (SELECT TOP 1
			Value
		FROM config
		WHERE type = 'sql-data-view'
			AND parentid = @ID
			AND value = @NewInsertDataViewTitle) BEGIN
			DECLARE @UniqueInsertDataViewTitleError varchar(3000) = 'A data view with the name ' + @NewInsertDataViewTitle + ' already exists in this table.';
			SELECT 'Error'[WebAPIMessageType], @UniqueInsertDataViewTitleError[WebAPIMessage]
			RAISERROR (@UniqueInsertDataViewTitleError, 20, -1) WITH LOG
		END

		INSERT INTO BigDataValues
			(Value)
		VALUES
			(@NewInsertDataViewSQL)

		DECLARE @LastInsertedDataViewSQLID int = SCOPE_IDENTITY();

		INSERT INTO Config
			(ParentID, DataLinkID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@ID, @LastInsertedDataViewSQLID, 'sql-data-view', @NewInsertDataViewTitle, @CreatedBy, @CreatedByID, GETDATE())

		DECLARE @LastInsertedDataViewID int = SCOPE_IDENTITY();

		INSERT INTO Config
			(ParentID, Type, Value, CreatedBy, CreatedByID, CreatedDate)
		VALUES
			(@LastInsertedDataViewID, 'sql-data-view-comment', @NewInsertDataViewComment, @CreatedBy, @CreatedByID, GETDATE())

	END

	--------------

	IF @What = 'data-view' AND @Action = 'update' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'sql-data-view')
			AND Type = 'table-right-designer'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table designer rights missing.'[WebAPIMessage]
			RAISERROR ('Table designer rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = ((SELECT ParentID
			FROM Config
			WHERE ID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'sql-data-view')))
			AND tab_right_group.Type = 'table-group-right-designer'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group designer rights missing.'[WebAPIMessage]
			RAISERROR ('Table group designer rights missing.', 20, -1) WITH LOG
		END

		DECLARE @NewUpdateDataViewTitle varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @NewUpdateDataViewComment varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);
		DELETE TOP (1) FROM @SplitTable
		DECLARE @NewUpdateDataViewSQL varchar(max) = (SELECT TOP 1
			RTRIM(LTRIM(val))
		FROM @SplitTable);

		IF EXISTS (SELECT TOP 1
			Value
		FROM config
		WHERE type = 'sql-data-view'
			AND value = @NewUpdateDataViewTitle
			AND ID <> @ID) BEGIN
			DECLARE @UniqueUpdateDataViewTitleError varchar(3000) = 'A data view with the name ' + @NewUpdateDataViewTitle + ' already exists.';
			SELECT 'Error'[WebAPIMessageType], @UniqueUpdateTableGroupNameError[WebAPIMessage]
			RAISERROR (@UniqueUpdateTableGroupNameError, 20, -1) WITH LOG
		END

		INSERT INTO BigDataValuesArchive
			(OriginalID, Value,ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT id, Value, @CreatedBy, @CreatedByID, GETDATE()
		FROM BigDataValues
		WHERE id = 
		  (select DataLinkID
		from config
		WHERE type = 'sql-data-view' AND ID = @ID)

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ID = @ID AND Type = 'sql-data-view'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'sql-data-view-comment'

		UPDATE BigDataValues set value=@NewUpdateDataViewSQL
		where id = (select DataLinkID
		from config
		WHERE type = 'sql-data-view' AND ID = @ID)

		UPDATE Config SET value = @NewUpdateDataViewTitle,
        ModifiedBy = @CreatedBy,
        ModifiedByID = @CreatedByID,
        ModifiedDate = GETDATE()
		WHERE ID = @ID AND Type = 'sql-data-view'

		UPDATE Config SET value = @NewUpdateDataViewComment,
		ModifiedBy = @CreatedBy,
		ModifiedByID = @CreatedByID,
		ModifiedDate = GETDATE()
		WHERE ParentID = @ID AND Type = 'sql-data-view-comment'
	END

	--------------

	IF @What = 'data-view' AND @Action = 'delete' BEGIN
		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config
		WHERE ParentID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'sql-data-view')
			AND Type = 'table-right-designer'
			AND (Value = @CreatedBy OR Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table designer rights missing.'[WebAPIMessage]
			RAISERROR ('Table designer rights missing.', 20, -1) WITH LOG
		END

		IF NOT EXISTS (SELECT TOP 1
			ID
		FROM Config[tab_right_group]
		WHERE
			tab_right_group.ParentID = ((SELECT ParentID
			FROM Config
			WHERE ID = (SELECT ParentID
			FROM Config
			WHERE ID = @ID AND Type = 'sql-data-view')))
			AND tab_right_group.Type = 'table-group-right-designer'
			AND (tab_right_group.Value = @CreatedBy OR tab_right_group.Value = 'Everyone')) BEGIN
			SELECT 'Error'[WebAPIMessageType], 'Table group designer rights missing.'[WebAPIMessage]
			RAISERROR ('Table group designer rights missing.', 20, -1) WITH LOG
		END

		INSERT INTO BigDataValuesArchive
			(ID, Value,ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT id, Value, @CreatedBy, @CreatedByID, GETDATE()
		FROM BigDataValues
		WHERE id = 
		  (select DataLinkID
		from config
		WHERE type = 'sql-data-view' AND ID = @ID)

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ID = @ID AND Type = 'sql-data-view'

		INSERT INTO ConfigArchive
			(OriginalID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			ArchivedBy, ArchivedByID, ArchivedDate)
		SELECT ID, ParentID, Type, Value, DataLinkID,
			CreatedBy, CreatedByID, CreatedDate,
			ModifiedBy, ModifiedByID, ModifiedDate,
			@CreatedBy, @CreatedByID, GETDATE()
		FROM Config
		WHERE ParentID = @ID AND Type = 'sql-data-view-comment'


		INSERT INTO BigDataValuesArchive
			(ID, Value,ArchivedBy,ArchivedByID,ArchivedDate)
		SELECT big.id, big.Value, @CreatedBy, @CreatedByID, GETDATE()
		FROM DataValues vals
			left outer join BigDataValues big on vals.BigDataValueID = big.id
		WHERE big.id = (select DataLinkID
		from config
		WHERE type = 'sql-data-view' AND ID = @ID)

		DELETE FROM Config WHERE id = @ID AND type = 'sql-data-view'

		DELETE FROM Config WHERE ParentID = @ID AND type = 'sql-data-view-comment'

		DELETE FROM BigDataValues WHERE id = (select DataLinkID
		from config
		WHERE type = 'sql-data-view' AND ID = @ID)
	END

--------------
END