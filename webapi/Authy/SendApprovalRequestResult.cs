﻿using System.Collections.Generic;
using Authy.Net;
using Newtonsoft.Json;

namespace AuthyOT
{
    public class SendApprovalRequestResult : AuthyResult
    {
        [JsonProperty("approval_request")]
        public IDictionary<string, string> ApprovalRequest { get; set; }
    }
}