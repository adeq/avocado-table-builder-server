ECHO OFF
CLS

ECHO b - Build
ECHO s - Send to production web server.
ECHO bs - b+s
ECHO ug - Update up_Get stored procedure.
ECHO uu - Update up_Update stored procedure.
ECHO.
SET /P M=Type an option then press enter: 


IF %M%==b GOTO c-b
IF %M%==s GOTO c-s
IF %M%==bs GOTO c-bs
IF %M%==ug GOTO c-ug
IF %M%==uu GOTO c-uu
@GOTO :EOF


:c-b
@ECHO ON
@ECHO Starting
@TIME /T
@DEL webapi\bin\AvocadoTableBuilderWebApi.dll
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\MSBuild.exe" AvocadoTableBuilderWebApi.sln 
@TIME /T
@ECHO Done
PAUSE

@GOTO :EOF

:c-s
@ECHO ON
@ECHO Starting
@TIME /T
XCOPY webapi\bin\AvocadoTableBuilderWebApi.dll "\\adeq-ns-iis\Intranet\webapis\table-builder\bin" /C /Y
@TIME /T
@ECHO Done
PAUSE

@GOTO :EOF

:c-bs
@ECHO ON
@ECHO Starting
@TIME /T
@DEL webapi\bin\AvocadoTableBuilderWebApi.dll
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\MSBuild.exe" AvocadoTableBuilderWebApi.sln 
@If Not Exist webapi\bin\AvocadoTableBuilderWebApi.dll EXIT
@ECHO.
@ECHO.
@ECHO Send to server?
@PAUSE
XCOPY webapi\bin\AvocadoTableBuilderWebApi.dll "\\adeq-ns-iis\Intranet\webapis\table-builder\bin" /C /Y
@PAUSE

@GOTO :EOF

:c-ug
@ECHO ON
@ECHO Starting
@TIME /T
sqlcmd -U dbadmin -S dpsqls -i webapi\up_Get.sql
@TIME /T
@ECHO Done
@PAUSE

@GOTO :EOF

:c-uu
@ECHO ON
@ECHO Starting
@TIME /T
sqlcmd -U dbadmin -S dpsqls -i webapi\up_Update.sql
@TIME /T
@ECHO Done
@PAUSE
